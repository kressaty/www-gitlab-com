---
layout: markdown_page
title: "GitLab and Google Cloud collaborate to simplify scalable app deployment"
---

### GitLab's integration with Google Kubernetes Engine (GKE) automatically configures a full DevOps pipeline and deployment environment with a few clicks

**SAN FRANCISCO, CALIF- April 5, 2018** - GitLab, a single application for the complete DevOps lifecycle, in collaboration with Google Cloud is offering [native integration into Google Kubernetes Engine (GKE)](/solutions/google-cloud-platform/). The integration enables GitLab developers to take advantage of [Auto DevOps](/blog/2017/09/22/gitlab-10-0-released/#auto-devops), by simplifying the complexity of setting up and deploying to a Kubernetes cluster.

As software development teams adopt modern [Cloud Native](/cloud-native/) development practices, using microservices and containers, they are increasingly turning to Kubernetes for container orchestration. But setting up and maintaining Kubernetes clusters can be time and resource intensive. GitLab's native GKE integration allows developers to automatically spin up a cluster to deploy applications, with just a few clicks. The clusters are fully managed by Google SREs and run on Google Cloud Platform's (GCP) best-in-class infrastructure.

"We’re excited to collaborate with GitLab to make GKE even more simple to set up through integration with GitLab’s automated DevOps capabilities," said William Denniss, Kubernetes Product Manager at Google. "We are constantly looking to further GKE's mission of enabling customers to easily deploy, manage and scale containerized applications on Kubernetes, and this collaboration with GitLab unlocks accelerated DevOps for containerized applications at scale."

"Before the GKE integration, Gitlab users needed an in-depth understanding of Kubernetes to manage their own clusters," said Sid Sijbrandij, CEO of GitLab. "With this collaboration, we've made it simple for our users to set up a managed deployment environment on GCP and leverage GitLab's robust Auto DevOps capabilities."

GitLab's Auto DevOps feature automatically configures CI/CD pipelines to build, test, verify, and deploy application code based on best practice templates. Although GitLab CI/CD can deploy to any environment from cloud to bare metal, Auto DevOps relies on having a Kubernetes deployment environment. With the GKE integration, now even the setup and configuration of Kubernetes clusters is automated. Businesses and developers that would have previously found self-managed Kubernetes, or even CI/CD, too difficult to set up can now realize the benefits of both.

Shipped in the release of [GitLab 10.6](/blog/2018/03/22/gitlab-10-6-released/), these new features are available today. To learn how you can get started with auto-configured DevOps pipelines that deploy to Google Kubernetes Engine with GitLab visit [/google-cloud-platform](/solutions/google-cloud-platform/).

**About GitLab**

GitLab is a single application built from the ground up for all stages of the DevOps lifecycle for Product, Development, QA, Security, and Operations teams to work concurrently on the same project. GitLab provides teams a single data store, one user interface, and one permission model across the DevOps lifecycle, allowing teams to collaborate and work on a project from a single conversation, significantly reducing cycle time, and focus exclusively on building great software quickly. Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprise organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel trust GitLab to deliver great software at new speeds.

**Media Contact**

Nicole Plati

[gitlab@highwirepr.com](mailto:gitlab@highwirepr.com)

415-963-4174 ext. 39
