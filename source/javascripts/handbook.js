(function() {
  var wrapper = document.querySelector('.wrapper');
  var container = wrapper.querySelector('.container');
  var markdownToc = document.getElementById('markdown-toc');
  var onThisPage = document.getElementById('on-this-page');
  var titles = container.querySelectorAll('h1, h2:not(.no_toc), h3, h4, h5');

  if (markdownToc) {

    var tocs = document.querySelector('.tocs-sidebar');

    // If the table of contents sidebar does not yet exist, create it.
    if (!tocs) {
      tocs = document.createElement('aside');
      tocs.className = 'tocs-sidebar';
    }

    // Create a new table of contents heading and add it to the sidebar.
    var tocHeading = document.createElement('h4');
    tocHeading.appendChild(document.createTextNode('On this page'));
    tocs.appendChild(tocHeading);

    // Clone the table of contents list and remove its id and class attributes.
    var tocClone = markdownToc.cloneNode(true);
    tocClone.removeAttribute('id');
    tocClone.removeAttribute('class');

    // Select just the cloned elements with id attributes.
    var clonedElementsWithIds = tocClone.querySelectorAll('[id]');

    // Remove the id attributes from the cloned elements.
    for (var i = 0; i < clonedElementsWithIds.length; i++) {
        clonedElementsWithIds[i].removeAttribute('id');
    }

    // Add the cloned and scrubbed table of contents list to the sidebar.
    tocs.appendChild(tocClone);

    wrapper.classList.add('handbook-wrapper');
    wrapper.classList.add('clearfix');

    // If the table of contents sidebar is an orphan, give it a nice home.
    if (!tocs.parentElement) {
      wrapper.insertBefore(tocs, container);
    }

    // Add responsive hiding classes to the original table of contents elements.
    markdownToc.classList.add('hidden-md', 'hidden-lg');
    onThisPage.classList.add('hidden-md', 'hidden-lg');
  }

  [].slice.call(titles).forEach(function(el) {
    el.innerHTML += '<a href="#' + el.id + '" class="handbook-md-anchor"></a>';
    el.classList.add('handbook-md-title');
  });

  // If a fragment identifier was specified and a corresponding element exists then make it visible.
  var fi = (document.location.hash) ? document.location.hash.substr(1) : null;
  var el = (fi !== null) ? document.getElementById(fi) : null;

  if (el) { el.scrollIntoView(); }
})();