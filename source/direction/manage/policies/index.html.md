---
layout: markdown_page
title: "Category Vision - Policies"
---

- TOC
{:toc}

## Policies: Introduction

Thanks for visiting the strategy page for Policies in GitLab. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/720) for this category.

For enterprises operating in regulated environments where maintaining control over permissions is critical, organizations typically rely on GitLab's roles and permissions controls to maintain acceptable controls. However, some organizations fitting this profile may consider GitLab's role system to be too broad and without the fine-grained controls they need to manage risk. For many customers, "they often resort to setting everyone to Maintainer and trusting their colleagues to not make mistakes" ([see research](https://gitlab.com/gitlab-org/ux-research/issues/62)). This is a painful state for instances - and for some, a hard barrier that completely prevents adoption of GitLab.

In almost all cases, these users want to *further constrain* existing roles, instead of permitting their users to do more. Without an ability to maintain a separation of duties between individuals at work, administrators for these organizations are faced with few workarounds: they can tolerate higher risk, lock down an instance and manage additional administrative overhead at the price of velocity, or build and maintain layers of automation on their own. For enterprises to thrive, this is a problem that demands a high-quality, native solution in GitLab.

## What's Next & Why

There'll never be a perfect role for every organization; as we've learned from [user feedback](https://docs.google.com/document/d/1-MZItci-Uxn4m-uEh5wR5VnC2sTeR_8UYdgaZmO6Yik/edit), organizations want customizable, deep control on a wide swath of settings and features. It's possible for us to iterate on our existing permissions framework by adding additional configuration and roles, but we're skeptical that we can offer the level of customization that can work for organizations with complex compliance needs.

We've considered [custom roles](https://gitlab.com/gitlab-org/gitlab-foss/issues/12736) in the past, but we feel there's a need for a new framework that can allow an instance to constrain roles without requiring the overhead and complexity of fully custom roles.

This category - Policies - is intended to be that solution; instead of writing custom roles, we'd like to be able to write custom conditions for what the existing roles in GitLab are able to do, under different conditions. We're inspired by [identity-based policies in systems like AWS](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies.html#policies_id-based) that allow policies to be defined and applied to resources we'd like to protect.

By introducing a layer of enforcement on top of our existing [RBAC](https://en.wikipedia.org/wiki/Role-based_access_control), this system lends us a high degree of flexibility in GitLab and reduces risk by adding to our permissions system instead of supplanting it.

The first step on this journey is introducing a Minimal, viable version of Policies - described further in our Maturity Plan below.

### How you can help

As we're still the conceptual stages of developing Policies, **your feedback is critical** to successfully shaping this category so that it works for your organization's needs. If you're interested in this problem space, please [comment on your needs and experiences with permissions in GitLab](https://gitlab.com/groups/gitlab-org/-/epics/1982). We're especially interested in:
* How are you currently working around permissions and compliance problems in GitLab?
* What specific controls would you like introduced?
* Which conditions would ideally determine which users are not able to take a particular action? Conversely, which conditions would ideally determine who is?
* Do you have any comments on the solutions we're exploring?

### [Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/720)

Policies is currently **Planned**. The next step in Policies' maturity is to **Minimal** maturity. 
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.

To achieve a minimal version of Policies, we'll conduct deep discovery on the problem space before taking our first step. An [MVC](https://about.gitlab.com/handbook/product/#the-minimally-viable-change-mvc) for Policies will likely allow an administrator or Owner to define a basic Policy and apply the ruleset to a particular project. We're fans of version control at GitLab, so we should be able to take a compliance-as-code approach that allows us to store these rules in a structured file.
* Please track the Minimal maturity plan in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1982).

Once we've achieved a minimal version of Policies, achieving a **Viable** level of maturity will involve collecting customer feedback and reacting. Assuming we're on the right track, we'll likely scale the solution in two dimensions:
* Increase the number of settings and features you can write Policies for. The MVC will likely focus on 1-2 common compliance use cases, and we'll need to increase the breadth of controls offered,
* Increase the number of user states that a Policy can use to permit/deny an action. Again, the MVC will likely focus on allowing/disallowing actions with only a few ways to write rules: likely allowing/disallowing a particular action for all users. We anticipate needing further depth, such as allowing an action if a user is a member of a particular group.

Finally, once we've achieved a ruleset that's sufficiently flexible and powerful for enterprises, it's not enough to be able to define these rules - we should be able to measure and confirm that they're being adhered to. Achieving **Complete** or **Lovable** maturity likely means further expansion of the two dimensions above, plus visualizing/reporting on the state of Policies across the instance.
