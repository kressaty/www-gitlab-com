---
layout: markdown_page
title: "Product Vision - Mobile"
---

- TOC
{:toc}

Developing and delivering mobile apps with GitLab is a critical capability. Many technology companies are now managing a fleet of mobile applications, and being able to effectively build, package, test, and deploy this code in an efficient, effective way is a competitive advantage that cannot be understated. GitLab is taking improvements in this area seriously, with a unified vision across several of our [DevOps stages](/direction/#devops-stages).

## North Stars

We apply our global product strategy to thinking about Mobile users. We treat the following principles as our north stars:

- **Single Developer Application** - GitLab will expand in breadth to be your mobile development hub.   
- **Concurrent DevOps** - All users throughout the mobile development lifecycle, whether they are in 
  QA, design, product management, security, release or support, will be able to contribute.
- **Lovable Experience** - Mobile developers deserve a tool that is friendly, slick, and makes them look like heroes.

## Stages with mobile focus

There are several stages involved in delivering a comprehensive, quality mobile experience at GitLab. These include, but are not necessarily limited to the following:

- [Manage](/direction/manage): Offering a shared MacOS runners fleet for doing iOS builds, comprehensive templates to get started quickly.
- [Create](/direction/create): Web IDE features that allow you to easily manage the kinds of code and artifacts you work with during mobile development.
- [Verify](/direction/verify): Runners for MacOS, Linux-based builds for iOS.
- [Package](/direction/package): Build archives for mobile applications.
- [Release](/direction/release): Review apps for mobile development, code signing and publishing workflows to TestFlight or other distribution models.

## Highlighted epics and issues

There are a few important issues you can check out to see where we're headed. We are collecting these in [gitlab-org&769](https://gitlab.com/groups/gitlab-org/-/epics/769).
