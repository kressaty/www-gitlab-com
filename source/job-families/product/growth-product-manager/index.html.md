---
layout: job_family_page
title: "Growth Product Manager"
---

## Role

We're looking for a product manager for our usage growth efforts.
You'll be the first team member of a new team within GitLab that focuses on
getting more people use more of GitLab.

In particular, we're looking for someone that is data-oriented and has prior
growth experience. It's not a must to have held a product manager position before,
but it's preferable. Having founded your own startup is a plus.

We work in quite a unique way at GitLab, where lots of flexibility and independence is mixed with a high paced, pragmatic way of working. And everything we do is in the open.

We recommend looking at our [primer](/company/) to get started.

## Responsibilities

- Identify low-performing features and workflows in GitLab and work towards improving these by
designing and conducting experiments, A/B tests, etc
- Help build a usage growth team by hiring data scientists and engineers
- Build and expand features that help our customers understand their own usage better and adopt
more of GitLab
- Create dashboards for actionable insight into any and all of GitLab's usage data
- Create and execute an event-driven analytics strategy
- Investigate reasons for churn and address these
- Help the rest of GitLab Inc use and adopt data tools
- Work together closely with the rest of the (product) team to help GitLab become more data-driven

## You are _not_ responsible for

- Shipping in time. As a PM you are part of a team that delivers a change,
the team is responsible for shipping in time, not you.
- A team of engineers. PMs at GitLab do not manage people, they manage the
_product_. You'll be required to take the lead in decisions about the product,
but it's not your role to manage the people that build the product.
- Capacity and availability planning. You will work together with engineering
managers on schedules and planning: you prioritize, the engineering managers
determine how much can be scheduled.

## Requirements

- Strong background in data science
- Proficient in SQL, specific experience with Postgres a plus
- Experience using tools such as Mixpanel, Amplitude, Looker
- Experience in product management
- Understanding of Git and Git workflows
- Strong technically. You understand how software is built, packaged and deployed.
- Passion for design and usability
- Highly independent and pragmatic
- Excellent proficiency in English
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- You share our [values](/handbook/values), and work in accordance with those values
- Bonus points: experience with GitLab
- Bonus points: experience in working with open source projects
- Bonus points: Machine learning experience
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

#### Responsibilities

- Plan and execute on improvements in GitLab related to growth
- Write specs and create wireframes to communicate your plans
- Ship improvements every month and make it possible to report on those
improvements
- Do data analysis whenever useful
- Assist the rest of the team with topics related to growth
- Build and expand tools related to growth (version.gitlab.com and others)

## Relevant links

- [Product Handbook](/handbook/product)
- [Engineering Workflow](/handbook/engineering/workflow)

## Primary Performance Indicators for the Role
[Growth Performance Indicators](https://about.gitlab.com/direction/growth/index.html)

## Hiring process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the VP of Product
* Next, second interview with a product manager
* Candidates will then be invited to schedule a third interview with a data engineer or manager
* Candidates may be asked to meet with additional team members at the manager's discretion
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
