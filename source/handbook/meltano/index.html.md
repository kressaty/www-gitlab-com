---
layout: handbook-page-toc
title: "Meltano"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Summary

[Meltano](https://gitlab.com/meltano/meltano) is a convention-over-configuration framework for analytics, business intelligence, and data science. It leverages open source software and software development best practices including version control, CI, CD, and review apps.

[View our roadmap](https://meltano.com/docs/roadmap.html)

[File a new issue](https://gitlab.com/meltano/meltano/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=&issuable_template=bugs)

## Opportunity

GitLab can build meaningful enterprise value with new products that win the hearts and minds of the next generation of collaborative knowledge workers. One growing group of professionals tasked with leveraging company’s data to improve their products, services and internal processes. Today, these people have job titles ranging from software developer roles like “data engineer” and “machine learning engineer” to semi-technical roles like “_______ analyst” and “FP&A”.

## Vision

Meltano has entered a crowded space late and needs to find points of differentiation to win the attention of potential users who are overwhelmed by the range of tools choices in this space. While there are many products solving specific problems in the “big data/analytics” space, there is not a clear winner when it comes to offerings that stitch together the entire end-to-end process.

## Mission

Integrate the best-in-class open source software to provide a single end-to-end solution for extracting data out of various sources (SaaS tools, internal databases, etc.), transforming them in human-usable data, performing analysis, and automating the process to make data refreshing and dashboard updating repeatable without being manual.

## Team

* [Danielle](https://gitlab.com/dmor) - General Manager
* [Douwe](https://gitlab.com/DouweM) - Engineering Lead
* [Micaël](https://gitlab.com/mbergeron) - Sr. Backend Engineer
* [Yannis](https://gitlab.com/iroussos) - Sr. Backend Engineer
* [Derek](https://gitlab.com/derek-knox) - Sr. Frontend Engineer
* [Ben](https://gitlab.com/bencodezen) - Sr. Frontend Engineer

## Code

See the active project here: [gitlab.com/meltano/meltano](https://gitlab.com/meltano/meltano)

Read our documentation here: [https://meltano.com/docs/](https://meltano.com/docs/)

## Releases

Meltano is released weekly on Mondays, and follows our [documented release procedure](https://meltano.com/docs/contributing.html#releases)

## Product

Our [milestones and scheduled work](https://gitlab.com/groups/meltano/-/milestones) is available to the public, and we encourage anyone to [submit new issues](https://gitlab.com/meltano/meltano/issues/new).

### Choosing What to Work On

We have limited engineering hours and focus available, and need to make sure we are working on things that will move Meltano forward towards our goal of increasing MAUI.

When evaluating a new major piece of work, we create an exploratory issue and use an opportunity assessment (some people call this "Market Requirement Doc" or MRD) to ask the following questions:

1. Exactly what problem will this solve? (value proposition)
2. For whom do we solve that problem? (target market)
3. How big is the opportunity? (market size)
4. What alternatives are out there? (competitive landscape)
5. Why are we best suited to pursue this? (our differentiator)
6. Why now? (market window)
7. How will we get this product to market? (go-to-market strategy)
8. How will we measure success/make money from this product? (metrics/revenue strategy)
9. What factors are critical to success? (solution requirements)
10. Given the above, what’s the recommendation? (go or no-go)

The [opportunity assessment was created by Marty Cagan at Silicon Valley Product Group](https://svpg.com/assessing-product-opportunities/)

### Product Cadence

We ship a new release every Monday, and always have a backlog of opportunity assessments to write and explore. However, given the stage of Meltano (pre-product/market fit) we don't plan specific work items more than a few weeks in the future.

### MR First

If you want to make an improvement to Meltano you don't have to wait for Product approval, kick-off some long convoluted dicsussion, or worry about stepping on anyone's toes. Submit a Merge Request (MR) with your proposed changes and we can iterate from there.

### "AND not OR" Mentality

As an internal startup we are constrained by our team size and the hours in the day, so we have to make smart trade-offs about the order and prioritization of what we do. Our goal is to chose the things that will unlock MAUI growth.

Sometimes, it can feel like we are chosing between two important things and this can be painful. However, we take the approach that anything is technically possible to build on the Meltano team so it's a just a question of the order of operations. On a long enough timeline, we will do everything we put on the roadmap -- so keep writing issues and hold onto that "it's an AND, not OR" mindset.

### Relationship to GitLab Product Org

Meltano operates independently within GitLab and is not part of the broader product organization. However, we learn quite a bit from [the approach of our teammates](https://about.gitlab.com/handbook/product/) and often adopt their processes. If you interact with our team and notice that Meltano Product functions differently, please know this is intentional.

GitLab's team and proccesses are designed for a scaled up startup with product market fit, which is a significantly different context than that of Meltano. We look forward to getting there someday!

## Community

We believe in building in public, and you can follow along with our progress:
* [Meltano blog](https://meltano.com/blog/)
* [Meltano YouTube channel](https://www.youtube.com/channel/UCmp7zJAZEC7I_n9BEydH8XQ?view_as=subscriber)
* [Meltano on Twitter](https://twitter.com/meltanodata)
* [Meltano community Slack](https://meltano.slack.com)

### Live Chat on Intercom.io

The [Meltano.com website](https://www.meltano.com) is set up with live chat powered by Intercom. When a website visitor to asks starts a conversation it will post in the [Meltano Slack instance](https://meltano.slack.com) in the #support-intercom channel, and Meltano core team members and community members can reply directly from there.

#### Intercom Accounts

Meltano team members each have basic accounts, and there is a shared account for whoever is on chat duty to log into with full access to the Inbox. Login credentials can be found in 1Password.

## Marketing

### Email

Meltano uses [Substack](https://meltano.substack.com/) to collect newsletter subscribers and send emails to communicate with our community. Email is opt-in, meaning that by default users can download and install Meltano without providing us any contact information.

### YouTube

Meltano posts numerous videos to YouTube each week, covering everything from product releases to engineering discussions. Meltano videos posted to YouTube should follow a standard template when naming a video and adding a video description. Common naming conventions make it simple to find the latest version of our weekly videos, and an expanded description with information about Meltano and how to contact the team make it easy for individuals who discover a video to get up to speed quickly.

#### Naming a YouTube Video

Videos uploaded to YouTube should have names that try and follow the same format wherever possible.

* They always start with “Meltano"
* They then describe the type of recording (Weekly Kickoff, Meeting, Speedrun, Release, Discussion, etc)
* If there’s a specific version of Meltano being referenced, (in a speedrun or release), reference it with a lower-case v followed by the release number (`v1.3.0`)
* Finish with the date in YYYY-MM-DD (`2019-11-01`)
* Separate all names with dashes

Examples of great YouTube video names include:

* Meltano Weekly Kickoff - 2019-10-31
* Meltano Release - v1.3.0 - 2019-10-31
* Meltano Meeting - Marketing Planning - 2019-10-31
* Meltano Discussion - Singer Tap Brainstorming - 2019-10-31

#### Writing a YouTube Video Description

Video descriptions should have a short (can be one sentence or phrase) blurb at the beginning, followed by evergreen information about Meltano that can simply be copied and pasted.

Things to consider when writing a great, short, video description:
* Descriptions are short and to the point, explaining what the video is covering and optionally the teams participating (engineering, marketing, leadership, security, etc)
* Don’t worry about “how” something is being worked on, discussed, solved - that’s what the video is for! Description should cover “what, who, why"
* Great descriptions highlight key words/phrases, especially when discussing specific integrations (“we’re discussing how to fix a bug with our Google Analytics data tap”) or an external component (“our data source for XYZ has a new feature”)

Examples of a great YouTube video description include:

```
Meltano v1.3.0 is publicly released, adding the ability to distill rocket fuel and manufacture widgets, with numerous bug fixes and improvements, including:
* bug fix #1
* improvement #1
* improvement #2
```

```
The Meltano Engineering team discusses the importance of tabs vs spaces in minified CSS comments when optimizing for compiled code readability.
```

This full example of a Meltano YouTube video description includes evergreen content that should always be included:

```
Meltano v1.3.0 is publicly released, adding the ability to distill rocket fuel and manufacture widgets, with numerous bug fixes and improvements, including:
* bug fix #1
* improvement #1
* improvement #2

--

Meltano is a free alternative to expensive SaaS tools, providing an integrated workflow for modeling, extracting, loading, transforming, analyzing, notebooking, and orchestrating your data. Meltano integrates the tools you use every day into a single data pipeline, including data from any CSV, Google Analytics, GitLab, Salesforce, Postgres, and much more.

Sign up for our free newsletter: https://meltano.substack.com/

GET STARTED WITH MELTANO
Project Home: https://meltano.com
GitLab: https://gitlab.com/meltano/meltano
Install Meltano: https://meltano.com/docs/installation.html
Tutorials: https://meltano.com/tutorials/

SUBSCRIBE for more videos: http://www.youtube.com/subscription_center?add_user=meltano

Join Us On Slack: https://join.slack.com/t/meltano/shared_invite/enQtNTM2NjEzNDY2MDgyLTZhY2QzYzkwNjYzNWY5Zjk5ZTE1ZGExNzE1NTFmMWJiM2E2ODVhMDFlYjc5YzVjMjllZTZlZDVjNWU2ZjNjNzQ
Blog: https://meltano.com/blog/
GitLab: https://gitlab.com/meltano/meltano
Twitter: https://twitter.com/meltanodata
Newsletter: https://meltano.substack.com/

Meltano
http://youtube.com/Meltano 
```

## Product

### Distribution Channels

#### DigitalOcean Marketplace

Meltano is available as a 1-Click App in the DigitalOcean Marketplace. This allows users to skip the installation and hosting steps, configure a Droplet and immediately begin using the Meltano UI in 60 seconds or less.

##### Linking to our DigitalOcean Marketplace Listing

It is important to form links to our listing in the following way:

`https://marketplace.digitalocean.com/apps/meltano?action=deploy&refcode=1c4623f89322`

This link contains the "deploy" command which allows existing DigitalOcean users to skip the marketplace listing page and go straight to deploying their Droplet. It also includes our referral code, which gets all new DigitalOcean users $50 of free credit over 30 days through the [referral program](https://www.digitalocean.com/referral-program/). We also receive $25 of credit for each new user who spends over $25, which offsets our DigitalOcean bill each month and helps us keep our operating costs low.

Users will find this link on the [Meltano Installation Docs page](https://meltano.com/docs/installation.html#digitalocean-marketplace)

##### Following the DigitalOcecan Brand Guidelines

It is important that we be a good partner, creating a win-win for each of us. Please refer to the [DigitalOcean Marketplace Vendor Guide](https://marketplace.digitalocean.com/vendors/getting-started-as-a-digitalocean-marketplace-vendor) for instructions on proper use of logos, language, etc.


