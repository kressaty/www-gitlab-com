---
layout: handbook-page-toc
title: "GitLab Event Information"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Events at GitLab

There are 3 groups within marketing who handle external events. Each group has a specific purpose. Please review each page for specific details.

* [Community Relations](/handbook/marketing/community-relations/evangelist-program/)
* [Corporate Events](/handbook/marketing/corporate-marketing/#corporate-events)
* [Field Marketing](/handbook/marketing/revenue-marketing/field-marketing/)

## Which events is GitLab already sponsoring?
* Internal tracking of events, please add our [Events and Sponsorship Calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9laWN2b3VkcHBjdTQ3bG5xdTFwOTlvNjU2Z0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) to your calendar.
* External facing event tracker can be found at [https://about.gitlab.com/events/](/events/)

## Suggesting an Event

To determine who would handle the event, please refer to our [events decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit#gid=0). If it is not clear who should own an event based on the decision tree, please email `events@gitlab.com`.

Please *only request* event support/sponsorship if your proposed event fits the following criteria:
The event will further business aims of GitLab.
The event has an audience of **250+ people** (the exception being meet-ups (which are run by our community team) or is part of an Account Based Marketing activity.
The event is a more than a month away.


* If your event fits the criteria above and you would like support from marketing, create an issue in the appropriate marketing project.
* [Community Relations](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=sponsorship-request)
* [Corporate Events](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?issuable_template=Corporate-Event-Request)
* [Field Marketing](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=Event_Field_Marketing)
  *  For AMER Field Marketing event requests, please select and utilize the [`AMER_Event_Request_Template`](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=AMER_Event_Request_Template)

Be sure you review the issue template and provide all necessary information that is asked of you in the issue.


## How We Evaluate and Build Potential Events
All GitLab events must check at least drive two or more of the aims of our events below to be considered.
 - Brand awareness- we want to be a household name by 2020!
 - Build community
 - Gain contributors
 - Thought leadership
 - Help with hiring
 - Gather new relevant leads/ drive ROI
 - Educate possible buyers or users on our product or features
 - Marketplace positioning
 - Partnerships/ Alliances

### Corporate events must also meet:
{:.no_toc}
  * Audience minimum requirement of 1000+ attendees and...
  * Audience demographic requirements. We consider the balance of roles represented (contributor, user, customer, potential hires), and the Global reach of the audience.


## Questions we ask ourselves when assessing an event:
- How and where will this position us as a brand?
- Does this event drive business goals forward in the next quarter? Year?
- Is the event important for the industry, thought leadership, or brand visibility? We give preference to events that influence trends and attract leaders and decision makers. We also prioritize events organized by our strategic partners.
- Will there be a GitLab speaker? We do not require a speaker slot in return for sponsorship but we do prioritize events where the audience will be hearing about GitLab - either from a GitLab team-member or a member of the wider GitLab community.
- What type of people will be attending the event? We prefer events attended by  diverse groups of decision makers with an interest in DevOps, DevSecOps, Cloud Native, Kubernetes, Serverless, Multi-cloud, CI/CD, Open Source, and other related topics.
- Will we be able to interact with attendees? We stress events that provide opportunities for meetings, workshops, booth and/or stands to help people find us, as well as create other interactions with attendees.
- Where will the event be held? We aim to have a presence at events around the globe with a particular focus on areas with large GitLab communities and large populations of support.
- What is the size of the opportunity for the event? We prioritize events based their potential reach (audience size, the number of interactions we have with attendees) and potential for ROI (also account for cycyle time).
- What story do we have to tell here and how does the event fit into our overall company strategy, goals, and product direction?
- Do we have the bandwidth and resources to make this activity a success? Do we have the cycles, funds, collateral and runway to invest fully and make the event as successful as possible? Event must be weighed against other current activity in region and department.


Suggested events will be subject to a valuation calculation - will it meet or exceed objectives listed above?

### For Corporate Marketing - Event Scorecard
Each question above is graded on a scale of 0-2. We then tally the scores and assign the event to a sponsorship tier.

- Events scoring below 8 are not eligible for corporate sponsorship or financial support.
- Events scoring 10+ are given top priority for staffing, and resources.

| Criteria / Score   | 0 | 1  | 2 |
|----------------|---------------|---------------|----------------|
| Thought Leadership |  |  |  |
| Audience type |  |  |  |
| Attendee interaction |  |  |  |
| Location and Timing |  |  |  |
| Event Relevance/ Strategy |  |  |  |
| Brand Reach |  |  |  |
| Opportunity size/ Potential ROI |  |  |  |

We ask these questions and use this scorecard to ensure that we're prioritizing the GitLab's brand and our community's best interests when we sponsor events.


## Event Execution
1. If the event is approved, the DRI will start finance issue for contract review and signature following the [Procure to pay instructions](/handbook/finance/procure-to-pay/). The finance issue will include:
     - ROI calculation for event in the final cost section.
     - Add the finance issue as a `Related issue` to the original issue for reference.
1. When contract has been signed the DRI will update issue label from `status:plan` to `status:wip`.
     - The designated MPM will begin the backend execution.
     - The MPM will create the event Epic, adding the checklist of related issues that need to be opened by respective team and high level information.
     - The MPM will also associate any issues opened for the event to the Epic.
1. For **Corporate events**, the DRI needs to create an Event planning issue, using "Corporate Event Planner Issue" template for tracking progress towards execution.
1. Add the event to Events Cal and Events Page. Instructions can be found here (need to add link).
1. Start checking off the appropriate issue template. Some things to note as you go through process in template:
     - Start an event specific slack channel.
     - Once team member staffing has been selected invite them to the channel in addition to other Field Marketing or Alliance team members that will be involved.
     - Do not link anything but the Epic in the slack channel.
     - Share the planning sheet for team members to add their contact and travel information.
     - Instruction everyone to book travel and lodging ASAP.
     - The planning sheet is used to track all details for travel, meeting setting, booth duty, speaker list, networking events, PR, etc.
1. The Epic is the main hub for all event information. **All issue** associated with the Event **must** be linked to the Epic!
     - Link the Original *Field* or *Corporate* marketing issue (some may refer to this issue as "META")
     - [Add the planning sheet](https://docs.google.com/spreadsheets/d/1i2-CdlsvW2x98NvJJ1mLcVq6ymehaRqSu2ckkWiV5ko/edit#gid=812678489&range=A1)
     - Link to event landing page
     - Add high level booth hours
     - Booth number (should be included in Epic name)
     - Any other high level informaiton that will be relevant to anyone attending
1. If the event needs a speaker, start an issue with the `Speaker Request` issue template and ping the tech evangelism team.
1. Landing Pages for Events ([detailed instructions]())
     - Corporate event landing pages are generated from the `events.yml` - [Detailed instructions]()
          - The MPM will create an issue for content to be provided working with Alliances &/or Product Marketing team on copy. 
          - A collaborative decision will be made to include a form on the landing page.
     - Owned events will use a landing page generated by the `events.yml` **OR** a Marketo landing page. ([Detailed instructions]()) Marketo will be used if **ALL** of the following criteria are met:
          - The event is **owned** by Field Marketing
          - The event will cost GitLab less than $50,000 USD (or your country's equivalent)
          - Promotion of the event will be no longer than 1.5 months
1. Schedule
     - Event kick off call scheduled approx **two months** out from event will include all people involved in planning
     - Final event check in meeting including everyone attending, involved Alliance team members and Technical Marketing team who created demos to review content with team.
     - Event recap will include all planners and stakeholders.
1. Copy needed
     - Landing page copy
     - Email invite copy - **3 to 4 weeks** in advance of event
     - Post Event Email copy - **1 to 2 weeks** in advance of event
1. Social
     - Start issue using the `Social Request` template for general social awareness posts and any social ads that need to be created.
     - For complete instructions on how to obtain social support at your event, please review the [social requests instructions](/handbook/marketing/corporate-marketing/social-marketing/#requesting-social-promotion-).
1. Design
     1. For the latest approved booth design & messaging, email `events@gitlab.com`.
     1. Open issue in the `Corporate Marketing` project for booth design. Assign to Design team and provide booth spec and due date. Provide as much notice as possible.
     1. For any content or major layout changes, tag Strategic Marketing and Design in the related booth design issue created in the `Corporate Marketing` project.
1. Digital
     - Coordinate all digital marketing requests with your regional MPM. See [Requesting Digital Marketing Promotions](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#requesting-digital-marketing-promotions) for more info.
1. For Corporate events - Meeting Setting
     1. Most **Corporate events** will have an onsite meeting setting initiative tied to the event goals.
     1. Corporate Marketing will work with the regional Field Marketing DRI and designated event MPM to decide on best meeting setting strategy.
     1. If any Executives are attending, all meetings scheduled for them will be coordinated through the designated EA for that event.
     1. Meetings are tracked on the master event spreadsheet.
          - The event spreadsheet will be locked 24 hours before the event starts.
          - Any changes need to be submitted by making a **Comment** on the spreadsheet and assigning it to the **Field Marketing** DRI.
     1. All on-site meetings must have a [meeting prep doc](https://docs.google.com/document/d/1hyA12EN5iEwApAr_g4_-vhUQZohKxm5xkX9xxZ1JNog/edit), which will be linked in the master planning sheet.
          - **NOTE**: We share these prep documents with the client. The document is intended to provide everyone attending the meeting with background information on the prospect &/or customer. The document should also include any objectives or topics to cover in the meeting.
     1. All leads gathered through meeting setting must be tracked in their own campaign which will be set up by the MPM and associated to the main event campaign.
     1. We generally provide a small give (under $50 USD or country equivalent) for anyone who takes a meeting with us.
1. Demos, booth decks and documentation
     - Product Marketing helps make all displayed collateral at events.
     - The [standard demos](/handbook/marketing/product-marketing/demo/) should be preloaded on the event iPads.
     - If you need something specific for an event, start an issue in the Product Marketing project and assign to Dan Gordon at least **three weeks in advance**.
1. Swag
     - Decide appropriate giveaway for the event and audience.
     - Coordinate ordering with one of the preferred swag vendors.
     - Order extra storage at the event if all swag will not fit witin the booth.
1. Leads and Campaign Setup
     - Field Marketing DRI is responsible for pulling, cleaning and sharing the lead list with the MPM and MktgOps within 24 hours of event close or as soon as received by event coordinators.
     - If the event had multiple parts (booth, happy hour, meetings, etc) each will have its own Salesforce campaign and [Member status progressions](/handbook/business-ops/resources/#campaign-type--progression-status).
     - Use template to standardize the data following the [list import guidelines](/handbook/business-ops/resources/#list-imports).


#### Important Planning Note
{:.no_toc}
The above planning list is not exhaustive - see planning issue template in field marketing project for most up to date list of tasks.


### How We Decide Who Attends Which Events?

* The event DRI determines how many staffers we need at the event and is responsible for ensuring the staffers are all set to attend the event.
* If the event is more enterprise-focused we try to send more marketing/sales. Regional Sales Managers in partnership with FM select team members based on who has the most potential contacts in the area or going to an event.
* If the event is more user-focused we will lean towards sending more technical people to staff and fewer sales.
* Suggestion for staffing: Field Marketing will evalute GitLabbers who live in the area that might be a good fit for the audience.
* We lean towards those who might be thought leaders, specialists, or more social for a specific show - i.e. if we are sponsoring an AWS show, we would like for a GitLab + AWS expert to staff the event.
* We aim to bring minimal team members to keep costs and disruption to normal workflow low. We take into account what value everyone will provide as well as coverage balance. Please check with the event DRI if you would like to or would like to suggest someone participate in an event.
* Once you have agreed to attend an event, you are not able to back out unless there is a customer facing obligation you need to tend to. We have this in place to avoid unnecessary rework on the event DRI’s behalf.
* A lot of times a technical sales resource needs to also be assigned to attend an event. In order to do so, please review the SA handbook for [instructions](/handbook/customer-success/solutions-architects/#when-and-how-to-engage-a-solutions-architect) on how to secure one of our awesome SA's. Tag the meta issue with technical-staff::required and once staffing attained change to label technical-staff::complete.
* All those attending will need their manager's approval.
* If you have been approved by the DRI and your manager to help staff an event, all your travel will be included during the time for the event/ expo days. You need to be onsite and ready to help out as soon as the first expo hall shift opens up and you may book travel any time after the expo hall closes. We will cover the night of lodging before the expo hall opens through to the night it closes. Any additional nights will need to be covered by the individual.
* Event staffing list will close 2 weeks for **field** events or 3 weeks for **corporate** events before commencement of the event.
* If you are not officially involved in the event as part of the sponsorship, we would still like to know you will be attending so we can include you in any activities surrounding the event. Please comment in the event specific slack channel notifying the Field or Corporate Marketing team of your plans to attend after obtaining approval from your manager. Please add a comment in the Event epic as well.

### Requesting Technical Staffing

Some events require technical staffing (Solutions Architects and/or others from the Customer Success team). To request technical staffing, do the following:

- Add the label `technical-staff::required` to the meta issue - this will make it appear on the [Event Technical Staffing board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1254947)
- Add a note in the staffing section how many people you need and any particular criteria
- Add one or more of the `technical-staff-type` labels to the issue to indicate which group(s) might be able to send staff. For example, if the event is on the west coast of the USA, consider adding `technical-staff-uswest` and `technical-staff-commercial`
- CC the SA manager for the group(s) indicated
- Once the staffing is solidified, change the label to technical-staff::complete so it no longer appears on the event staffing board(s)

### Event Outreach
It is important that we are communicating with our customers and prospects when are attending an event. This is done through a targeted email sent through Marketo & also through SDR & SAL outreach.

- Receive attendee list and contact customers and prospects before event using talking points provided by content DRI with the goal of setting up meetings/ demos at the event. Invite them to anything specific we have happening at or around event.
- If there is not an attendee list process is as follows:
 - Target speakers for outreach.
 - Utilize previous years attendee list that can be found in SFDC. That person may not be attending, but their colleague might be. Ask for intros.
 - Follow event hashtags to see who will be attending.
 - Join local meetup and pre event events (this works well for large events like AWS).
 - Join LinkedIn Groups and slack channels dedicated to event.
 - Download event app and engage with attendees via app.
    - You can search for relevant talks and see who has registered for talks that might apply to potential customers.

#### Email alias usages for outreach
We use several email aliases for pre and post event outreach.  Below are guidlines as to which one should be used based on the event
 - Corporate events (all communications): `info@`
 - Field marketing events (invitations and follow up emails): `info@`
 - Field marketing event day before reminders: `fieldmarketing@`

#### Pre-Event Registration Tracking & Reporting    

As propects register for the event the Field Marketing team has the ability to track any potential dietary concerns: 

- [LEAD report](https://gitlab.my.salesforce.com/00O4M000004dr8v)
- [CONTACT report](https://gitlab.my.salesforce.com/00O4M000004dr95)

If the FM DRI for an event needs to reach out to someone prior to the event, they will do so leveraging their personal `@gitlab.com` email address & cc `fieldmarketing@` alias.  

The `Dietary Restrictions` field can be leveraged as a list filter by the MPMs if they are needing to send a bulk email of any kind. 

#### For Field Events
- Adding Records to the Campaign in order for the record to receive an invite to the event:
- SALs/SDRs should add members to campaign to be invited to event, using the appropriate `Campaign Status`:  
    - `Sales Invited` = You have personally invited the person to the event.
    - `Sales Nominated` = Marketing will invite the person on your behalf. They will receive invitation email pre-event plus any confirmations/reminders if they register.
    - `Marketing Invited` = Marketing will be sending geo-targted invitation emails to specific events pulling the names from our active database. Anyone included in this master send will be added to the campaign with this status.
    - **Any other Status** = Do not assign any other status to records. The campaign members will be updated by MPM or automated through registration.


#### Employee Booth Guidelines

- Perfect your Pitch
   - Most people have two ears and one mouth. Successful pitching is two parts listening to one part talking. Be engaged and interested in folks that visit. Be genuinely curious about their story. Understand them first before you start telling them about us.
   - A great way to start is to offer a handshake and say,
       1. "Hi, my name is [your_name]”
           1. “Hi I’m [their_name]”
       1. “[their_name, are you familiar with GitLab?”
           1. “Yes”
               1. “Thanks for using us, are you using just the source code management or are you also using the built-in GitLab CI/CD?”
           1. “No” - “Great, in a nutshell we <140 character description> - for example, what company do you work for?”
               1. “Company X”
               1. And what’s your role at company x?
                   1. “Title X”
               1. Tailor your pitch to their specific experience. Ask about what tools they are using today, what they like or dislike about those tools.
   - Working at the booth is a great place to try out different ways of explaining technology and trying out different value propositions to see what resonates the most.
   - Know some stock answers
   - You can ask them which talk they’ve heard so far has been the most interesting.
- Close the deal
   - Figure out what the next step for this person is. Are they a decision maker at a large org?
   - If a conversation is running long, get his/her info and schedule a time to chat or follow up at a later time outside of the booth. The goal of the booth is to make initial contact and connections.
  - Give a personal follow up
- Stand at the front of booth facing the crowd.
   - Don’t make folks walk into the booth and seek you out. Stand out at the front. Make eye contact and smile at folks walking by. If they stop or pause you can ask them, “Are you familiar with GitLab?”
- Hangout outside the booth.
   - Too many GitLab team-members in the booth discourages other folks from coming by. The booth can be a great place to meet up, but don’t hang out there. Move the conversation to a near by lounge or social area.
   - When you are at the booth keep conversation with your coworkers to a minimum.
   - When you are at the booth focus on serving the attendees.
   - Do not do normal daily work in the booth - the booth is not a place for taking calls, or responding to emails. When you are at the booth you are on booth duty and that is it.
- Keep an abundant tidy stash of swag out
   - During slow times, restock swag and tidy up booth.
- Keep the booth clean.
   - The booth should be clean and organized at all times.
- Avoid eating meals in booth, please keep lids on beverages and out of sight.
- If we have the bandwidth or the traffic is slow do not be afraid to walk around to other booths and talk to people. Make friends we could partner with, create interesting content with, or just have friendly beers.
- If press comes to the event feel free to put them in contact with CMO.
- Engage the competition.
   - Be friendly and polite to competitors to come by the booth.
- Don't forget your business cards.

#### Scanning Best Practices
- Be an active but polite Badge Scanner  
   1. Don’t reach for the badge without first asking if you can scan someone’s badge and don't lead a conversation with can I scan you. Ask folks politely, “Would you appreciate a follow up email?” or “Mind if I scan your badge?” Many folks will say, “yes.” If they say, “Not really.” You can say, “Great, we don’t want to clutter up your inbox. You can always go online to about.gitlab.com if you’d like to check back in with us.”
   1. Trade scans for all swag if the attendee consents. If we have the space and staff, someone should be in charge of distributing and organizing swag, and scanning folks who come by for swag.
- Take good notes:
   1. Your initials (This way the email can be more personalized to say, “We saw you chatted with [name] in the booth.”) Add your initials + “follow up” if you personally can send them an email within a week of the conference.
   1. The tech stack (what tools they are using)
   1. Any specifics needed in the follow up (schedule a call, send docs for X, interested in Y, etc.)

##### Suggested Attire  

- Wear at least one piece of branded GitLab clothing. If you prefer to wear something dressier than the GitLab branded items available that is also acceptable. Feel free to wear our sticker on your clothing.
- If the conference is business casual try some nice jeans (no holes) or dress pants.
- Clean, closed-toed shoes please.
- A smile.

##### Booth Set Up  
- Bring:
   - Generic business cards
   - Stickers + any other swag
   - Events laptop (for slideshow) + charger + dongles
   - Backup power banks
   - Mints & hand sanitizer
   - One pagers + cheat sheets

##### Day of Booth Staffing   
- Ideally booth shifts will be around 3 hours or less.
- Staff more people during peak traffic hours.
- Avoid shift changes during peak hours.
- Aim to staff the booth with individuals with a variety of expertise and backgrounds- ideally technical and non-technical people from various departments should be paired.
- Send out invites on the Events & Sponsorship calendar to booth staff with the following information:
   - Time and date of event, booth, and shift
   - Any instructions on using or locating lead scanner
   - Any relevant event set up or clean up

#### Post Event  

- Add event debrief to event issue in marketing project. The debrief should include the following if applicable:
   - Was the event valuable?
       - Would you go again? Should we go again?
       - Did we get good leads/contacts? What was the audience profile like?
       - Best questions asked and conversations. Trends in questions asked.
       - Was our sponsorship/involvement successful? Did we go in at the proper sponsorship level?
   - How was the booth set up?
       - How was the booth staffing?
       - Did the booth get enough traffic?
       - Booth location and size
   - How did our swag go over?
       - Did we have enough/too much?
   - Contests
       - Did the contest(s) effectively build our brand and connecting with our target audience?

#### Event List  
1. List received by FMM from event organizers  
1. FMM reviews and cleans up list following the guidelines for [list imports](/handbook/business-ops/resources/#list-imports)
1. List is sent to Marketing Ops for upload to Marketo & associate to related Campaign (w/in 24hrs of receipt from event)
     * Marketo will match based on `Email Address` to existing records regardless if LEAD or CONTACT object.
1. Marketo will sync to SFDC automatically. LeanData assigns records based on Territory ownership.
1. Marketing Ops will create a LEAD & CONTACT view in SFDC for the SDR team to facilitate follow up.
1. Marketing Ops notifies MPM/FMM in the list clean issue when the upload has been completed so the general follow up email can be scheduled.  
1. Marketing Ops makes a post on the `#sdr_global` slack channel with a link to SFDC campaign, link to SDFC LEAD & CONTACT view, and link to Outreach view (if exists).
     * Ops will ping the `sdr` slack alias and cc the `@sdr-leadership` slack alias.

Common lead questions:
- Record ownership will be assigned using the [Global Ownership](/handbook/business-ops/resources/#global-account-ownership) rules
- All followup needs to be tracked in SFDC  
- List upload needs to be done **before** follow up is done so we can ensure proper order of operations & attribution is given correctly  
- Record Owner and/or SDR doing follow up need to be sure to update the [`Contact Status`](/handbook/business-ops/resources/#lead--contact-statuses) on the record as follow up is done.
- Campaign type & meaning of [Campaign Member status](/handbook/business-ops/resources/#campaign-type--progression-status) can be found in the Business Ops handbook

#### Marketo Check-in App
For **OWNED** events, the Marketo program is pushed to the Marketo Check-in app that allows the FMM team to be self-sufficient when it comes to updating an event registration list.

**To Activate Marketo-Check-in App**
1.  MPM to navigate to the Marketo program event
2.  At the top of the page, change `View: Summary` to `View: Schedule`
3.  You should see the event on the right side of the calendar
     ^ Events are displayed intially based on the program **`created date`**, not the event date
4.  Double click your event, update `date`,`end`, and event times (remember all times are based on Pacific Time)
5.  Click the  slide bar to change event from `tentative` to `confirmed`. Slide bar will change from *gray* to *orange* when done correctly.
*The event will not be visible in the Marketo Check-in app until 7 days prior to the event*

If you need support, use the #mktgops slack channel.

## Swag
- Swag selection and creation is managed by Corporate Marketing. All information on swag can be found in the [Corporate Marketing handbook](/handbook/marketing/corporate-marketing/#swag). For event related swag, we will make use of three evergreen campaign tags - Swag_Field, Swag_Corporate and Swag_Community, for spend tracking purposes.


## Specifics for Community Relations
## Specifics for Corporate Events
## Specifics for Field Events
- For events where a field marketing representative cannot be present, the FM DRI will assign an onsite lead. The DRI will be responsible for coordinating with this person and providing he/she any info they will need to help run the event in their absence. This person will be the venue point of contact as well as responsible for set up and tear down.
- FOR EMEA: We must ensure we are gathering GDPR compliant leads - Lead devices scanning follow up needs to be in event T&C. If GDPR is not in the T&C, we are not allowed to follow up on the leads. Scanning a lead is not automatically GDPR compliant if visitors have not agreed to it.

### AMER Field Marketing Swag
The AMER Field Marketing team utilizes our swag vendor, Nadel. Nadel is available to produce, ship and store swag and event material. The FMM is responsible for accessing the [Nadel Portal](https://rhu270.veracore.com/v5fmsnet/MONEY/OeFrame.asp?PmSess1=2095144&Action=LOGIN&pos=GIT476&v=4&OfferID=&sxref=) via the Field Marketing team login available in the 1Password Marketing Vault to order all swag and trade show items for their events.

Nadel Portal Demo - [View Here](https://drive.google.com/open?id=1JGGjmWioXmwKI8t1zowqyfl22GKeaOk3)    
Nadel Admin Portal Demo - [View Here](https://drive.google.com/open?id=1YNNjr-A8OJVLuod27vWfTCbEO1y21d9T)
- **Existing Items:** Item quantities are listed in the portal. Please select from the current items in stock for your event. If you need a larger quantity of an item over what is available, please reach out to `@krogel` for reordering.
- **New Items and Designs:** Requests for new swag items not already available in the Nadel portal require management approval. Any new swag designs must be approved by the brand team for brand consistency. Nadel will email all final designs to the brand team for approval. You can suggest new designs in the swag slack channel or more formally in an issue in the [Swag Project](https://gitlab.com/gitlab-com/swag_suggestions).
- **Large Orders:** For orders over 500 pieces, please do not pull from existing stock in the Nadel portal. Contact `@krogel` for assistance on placing a new order.
- **Shipping:** Please make sure to specify all shipping requirements and deadlines when ordering your items. Nadel will provide return shipping labels with each order for easy return shipping to the warehouse after your event.
- **Lead Times:** Please be aware that ordering newly designed swag or placing reorders for existing items requires adequate lead time. Timeframes vary greatly based on the items selected and design approval. General Lead Times: 6 weeks to produce a new item and 2-3 weeks to reorder current designs.
- **Stickers:** For sticker orders, please utilize our [Sticker Mule](https://www.stickermule.com/) account. Delivery options and timelines are provided during the ordering process. Any new sticker designs must be approved by the brand team for brand consistency. You can suggest new designs in the swag slack channel or more formally in an issue in the [Swag Project](https://gitlab.com/gitlab-com/swag_suggestions).

### AMER Sales Swag Requests
For AMER sales swag requests, please utilize the [`AMER_SwagRequest`](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=AMER_SwagRequest) template. **Please note that swag requests should be submitted at least two weeks in advance to allow time for approval, processing and shipping.**

### AMER Field Marketing Venue Search and Contract Requests
For venue search and contract requests, the FMM can open an issue utilizing the [`AMER_VenueSearch_ContractRequest`](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=AMER_VenueSearch_ContractRequest) template. Follow the instructions to provide event details and assign to the FMC.  

### AMER Content Event Deadline Process for MPM Tasks
* FMC is auto-assigned to event when FMM selects appropriate event template
* FMC attaches `FMC AMER - Event Tracking` label to main event issue
* MPM creates epic and related issues and assigns to FMM and FMC
* FMC adds all content deadlines for MPM issues to main event issue
* FMC to confirm that each email issue has a calendar event created in the `Email Marketing Calendar` with a link to the issue, and that `add to calendar` has been checked in the issue
* FMM to update Copy Document file with content and keep the Copy Document link in the epic
* For event issues that include `Follow up: planned from Field Marketing`, FMC to link follow up issue in this section once follow up issue has been created
* FMM checks off tasks and pings the MPM in the corresponding MPM issue as they are completed
* One (1) business day before task is due, FMC to ping FMM in corresponding MPM issue with content reminder

### AMER Corporate card usage 
* At times we need to use the Corporate Credit card to pay for events or goods to be used at events/related to events. The corp card number and all related information is stored in the Marketing 1pass. You are welcome to use this card, but you MUST send your receipts to the card holder via email and cc `fieldmarketing@`. You also need to include the event issue the expense is related to so the card holder knows which Net Suite charge code needs to be back charged.

### Lightning meetings with CXO

At GitLab owned events we will sometimes host 10 minute lightning meetings with the GitLab CXO(s) in attendance. We do this to facilitate many interactions with our leadership in a short amount of time.

#### Process
- Written & verbal announcement is made to audience either prior to the event in the Know before you go email or the announcement is made onsite at the event.
- The location should be clearly communicated to the audience as well as the 10 min time limit.
- The DRI for these meetings is responsible for writing down the attendees name and company as attendees arrive to form the queue of folks who will be meeting with the CXO.
- This should be done in the shared google sheet event briefing in a separate tab.
- The DRI can either stay in the room for the duration of the meeting or can step out to continue checking other attendees in.
- With 8 mins left, the DRI should begin to bring the meeting to a close by sensing the flow of the conversation and politely interrupt stating there is time for 1 more question.
- At the 10 minute mark, the DRI will politely cut the meeting off.
- The DRI walks out the attendee and then brings in the next.
- This process continues until the time limit has been reached.

#### Space considerations
- If possible there should be a location separate from the meeting space where the attendees can wait their turn to meet with the CXO.
- Consider offering light snacks and beverages as people wait.
- Have CXO and meeting attendee on the same level, physically, vs. one party sitting higher or standing and vice versa.

#### Things to consider
- If people would just like to meet the CXO and don’t need to take the full 10 mins for the meeting that is great! The max time is 10 mins.
- There is no agenda for the 10 min meeting. The DRI for the meetings should quickly research the company via SFDC (if you don’t have the SFDC app on your phone, this is a great use case for why it’s useful!) gather as much info as quickly possible and relay this info to the CXO. As an example, if there is an enterprise customer next in-line to meet with CXO, let the CXO know. Short brief info, as you won’t have much time to relay this info.
- Notes should be taken by the DRI or a designated delegate in the room so the CXO can focus on the meeting. These notes should be added within 48 hours to the person record in SFDC by the DRI note taker. If any next steps were discussed and an action was assigned to someone not in the room, be sure to tag the person who was assigned!

## How to add events to [about.gitlab.com/events](/events/)

In an effort to publicly share where people can find GitLab at events in person throughout the world, we have created [about.gitlab.com/events](/events).  This page is to be updated by the person responsible for the event. To update the page, you will need to contribute to the [event master.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/events.yml).
If you need more information about our exact involment in an specific event please visit the marketing project in gitlab.com and search the name of the event for any realted issues. The "Meta" issue should include the most thorough and high level details about each event we are participating in. Place your event in the order in which it is happening. The list runs from soonest to furthest in the future.
Save event images and headers here: Save images for featured events [here](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/images/events)

### Details to be included (all of which are mandatory in order for your MR to pass the build):
{:.no_toc}

- **Topic** - Name of the event you would like to add
- **Type** - Please choose one of the following: `Diversity`, `Conference`,
`MeetUp`, `Speaking Engagement`, `Webinar`, `Community Event` or `GitLab Connect`. **Events cannot have more than one type.** If more than one apply, choose the best. If you feel your event doesn’t fit in the below category, do not just manually add a type. Please reach out to events@gitlab.com  to suggest a new type of event.
- **Date** - Start date of event
- **Date ends** - Day event ends
- **Description** - Brief overview about event (can be taken from event homepage).
- **Location** - city, state,provinces, districts, counties (etc depending on country), country where event will take place
- **Region** - `AMER`, `LATAM`, `EMEA`, `APAC`, or `Online`
- **Social tags** - hashtag for event shared by event host
- **Event URL** - homepage for event

#### Example
{:.no_toc}

```
- topic: The Best DevOps Conference Ever
  type: Conference
  date: January 1 - 3, 2050
  date_ends: January 3, 2050 # Month DD, YYYY
  description: |
               The Best DevOps Conference Ever brings together the best minds in the DevOps land. The conference consists of 3 full days of DevOps magic, literally magic. Attendees will have the opportunity to partake in fire talks and moderated sessions. This is one you won’t want to miss.
  location: Neverland, NVR
  region: APAC
  social_tags: DEVOPS4LIFE
  event_url: https://2050.bestdevops.org
```

#### Template
{:.no_toc}

```
- topic:
  type:
  date:
  date_ends:
  description:
  location:
  region:
  social_tags:
  event_url:
```
For featured events include:
```
featured:
    background: background/image/src/here.png
```


## Creating an event specific landing page

**All landing pages** require the involvement of a Marketing Program Manager (MPM) as there are required steps to set up programs & tracking in both Marketo/Salesforce to correctly manage inbound submissions from the landing page.

For corporate tradeshows we will want to create an event specific page that links from the [about.gitlab.com/events](/events/) page. The purpose of this page is to let people know additional details about GitLab’s presence at the event, how to get in touch with us at the event, and conference sessions we are speaking in (if applicable).  

For select Field Marketing events, that meet the critera below, a Marketo landing page is used instead of an `events.yml` created landing page. By doing this, the MPMs own the creation of these pages and they are the only ones who will have edit access to these pages.

When to specifically use a Marketo landing page vs. the events yml:
1. This is an event owned by Field Marketing.
1. The event cost the company less than $50,000 (or your country's equivalent).
1. We will be driving traffic to the marketo landing page for less than 1.5 months.

Steps to take to create the new `events.yml` generated landing page:

1. Create new a new branch of the [www-gitlab-com project.](https://gitlab.com/gitlab-com/www-gitlab-com). - Branch name should be what event you’ve added.
1. From new Branch, navigate to `Data`, then to `events.yml`
1. Scroll down to the area where its date appropriate to add the event
1. Add event using instructions in [handbook](#how-to-add-an-event-to-the-eventsyml)
1. To create the event specific page you need to add a subset of the following information:

  - **url:** - you make this up based on what you want the URL to be from about.gitlab.com
  - **header_background:** choose from an image already in the images folder or add your own image. If you do not know how to do this, please watch [this tutorial](https://drive.google.com/open?id=14wCjHZLbcUUDArGYBNeinsHAlET6ubMy).
  - **header_image:** choose from an image already in the images folder or add your own. (optional- if you prefer not to include, remove field altogether)
  - **header_description:** what CTA would you like the person to do on the page
  - **booth:** booth number at event, if there is no booth number, then remove this line of code (optional)
  - **form:** code that tells the system to add the contact info form. Marketing Ops will provide you with this number. They need to create a specific form for each page associated with a campaign in sfc.
  - **title:** CTA for why someone would want to give their contact info. Also used in `contact:` to distinguish a header title.
  - **description:** additional info on why someone would want to give their contact info
  - **number:** Marketo form number - Marketing Operations will need to give this number to you. Under `form:`
  - **content:** all of the information in the example section is all optional based on your event. If its not needed, simply delete.
1. Please watch [this tutorial](https://drive.google.com/open?id=14wCjHZLbcUUDArGYBNeinsHAlET6ubMy) for additional help.

### Example
{:.no_toc}

```
- topic: AWS re:Invent
  type: Conference
  date: December 2-6, 2019
  date_ends: December 6, 2019
  description: |
             AWS re:Invent 2019 is the Amazon Web Services annual user conference dedicated to cloud strategies, IT architecture and infrastructure, operations, security and developer productivity.
  location: Las Vegas, NV, USA
  region: AMER
  social_tags: AWSreInvent2019
  event_url: https://about.gitlab.com/events/aws-public-sector-summit/
  # Giving the following data will give this event it's own dedicated page on about.gitlab.com, must provide a unique url.
  # If it is text, it needs to be wrapped in "double quotes". This is so you can use characters like : and ' without breaking anything.
  url: aws-reinvent
  header_background: /images/blogimages/gitlab-at-vue-conf/cover_image.jpg
  header_image: /images/events/aws-reinvent.svg
  header_description: "Drop by our booth to see a demo and speak with our GitLab experts!"
  booth: "S1607"
  form:
      title: "Request a meeting!"
      description: "Let us show you how GitLab can impact your business."
      number: 1691
      success message: "Thank you for requesting to meet! We'll be in touch shortly with more information."
  content:
    - title: "Make sure to stop by the GitLab booth at AWS re:Invent!"
      body: "Speak with our experts and learn how GitLab simplifies your deployment pipeline to accelerate delivery by 200%. See a live demo, learn about our latest releases, and explore what’s on the roadmap for GitLab."
    - title: "Request a meeting"
      body: "Fill out the form to request a meeting with GitLab. We'll share how we can impact your business as a complete DevOps platform, delivered as a single application. From project planning and source code management to CI/CD, monitoring, and security."
    - title: "Activities at AWS re:Invent"
      list_links:
        - text: "Register for our happy hour!"
          link: "#"
        - text: "Join our speaking sessions - see details below."
  speakers:
    - name: "Priyanka Sharma"
      title: "Director of Technical Evangelism"
      image: /images/team/priyankasharma-crop.jpg
      date: "Wednesday, May 22"
      time: "14:00 - 14:35"
      location: "Hall 8.0 F5"
      topic: "[The Serverless Landscape and Event Driven Futures](https://kccnceu19.sched.com/event/MPeI/the-serverless-landscape-and-event-driven-futures-dee-kumar-cncf-priyanka-sharma-gitlab?iframe=no&w=100%&sidebar=yes&bg=no)"
      description: "Serverless design patterns have grown in popularity amongst developers and enterprises alike and the ecosystem is exploding. Developers like moving faster by focusing on business logic without worrying about the underlying infrastructure. Today, there are umpteen solutions and OSS projects in the market and the space needs some organization to maximize effort. There is a lot of curiosity and confusion around serverless computing. What is it? Who is it for? Is it a replacement for IaaS, PaaS, and containers? Does that mean the days of servers are over? The CNCF created the Serverless Working Group to explore the intersection of cloud native and serverless technology. The first output of the group was creation of serverless landscape. The landscape lists some of the more common/popular Serverless projects, platforms, tooling, and services."
    - name: "John Jeremiah"
      title: "Director, Product Marketing"
      image: /images/team/johnjeremiah-crop.jpg
      date: "June 12, Wednesday"
      time: "1:25 PM–1:45 PM"
      location: "Partner Pavilion"
      topic: "Accelerating Speed to Mission: A Digital Transformation"
      description: "Velocity and speed of execution determine the winners. The faster your software teams can deliver, the bigger your advantage. The traditional software development processes have multiple layers of friction, checkpoints, and bottlenecks, often making simple projects complex, expensive, and lengthy. One of the hardest parts of delivering software is keeping everyone aligned and focused. Teams waste time waiting for inputs, fixing mistakes, shifting from one tool to another, waiting for infrastructure, and maintaining complexly integrated toolchains; this all creates friction and slows innovation. It doesn't have to be slow, learn how to accelerate your software delivery and increase your mission velocity."
```

### Template
{:.no_toc}

```
- topic:
  type:
  date:
  date_ends:
  description: |

  location:
  region:
  social_tags:
  event_url:
  # Giving the following data will give this event it's own dedicated page on about.gitlab.com, must provide a unique url.
  # If it is text, it needs to be wrapped in "double quotes". This is so you can use characters like : and ' without breaking anything.
  url:
  header_background:
  header_image:
  header_description:
  booth:
  form:
      title:
      description:
      number:
      success message:
  content:
    - title:
      body:
    - title:
      list_links:
        - text:
          link:
        - text:
          link:
        - text:
          link:
  speakers:
    - name:
      title:
      image:
      date:
      time:
      location:
      topic:
      description:
    - name:
      title:
      image:
      date:
      time:
      location:
```

## Creating a Marketo Landing Page
1. Go to Marketo and clone the template that matches your event type and follow the standard process for Marketo Program creation, SFDC sync, etc.
2. Fill in tokens by clicking the Marketo program > `My Tokens`
3. Click `Landing Page` in the Assets folder then click `Edit Draft`
4. Click `Landing Page Actions` > `Edit Page Meta Tags` > change Robots field to `index, follow`
5. Use the copy doc provided in the event Epic to create your page
6. When the landing page is finished, click preview to ensure all tokens are filled and the page looks ready for event traffic. Click `Preview Actions` > `Approve and Close`. You may need to update a token field or remove if you still see tokens and not the associated text
7. Update URL by clicking `Landing Page Actions` > `URL Tools` > `Edit URL setting`. New URL should reflect event name. (example... https://pages.gitlab.com/all-things-open)
8. Edit any related emails, such as `confirmation` or `sales alert` and approve
9. Review flow steps and activate related smart campaigns for lead routing 
10. Navigate to your Landing Page outside of Marketo and test the form. Ensure you receive confirmation email and alert (if applicable) and make sure your lead shows up in the Marketo campaign. 
11. Send link to event owner in `Landing Page Issue` for review and approval
11. FMM should review the page for grammatical errors, event details, test the form, and provide feedback or approval
11. Once approved by event owner, MPM add page to `/events`

## Email Process for Events
**Email Approval:**
1. MPM send yourself a test email and check that it is ready for FMM to review/approve
2. If email is ready for FMM approval, Click 'print', select 'Save as PDF' as your destination. Click 'More settings' and check the Options box for Background graphics, then save file (example - 20191016_Roadshow_NYC_reminder).
3. Add file to the description in your related issue (invitation-reminder or follow-up-email)
4. MPM to @ mention the FMM owner to let them know the email has been created and is in the description
5. FMM will review email for grammatical errors, event event details, and links and provide feedback and/or approval.
5. FMM will provide @ MPM for any requested changes, and MPM will make update and follow the process again.

## Speaking at events
If you’re looking for information about speaking at an events head over to our [Corporate Marketing page](/handbook/marketing/corporate-marketing/#speakers) for complete details.  
