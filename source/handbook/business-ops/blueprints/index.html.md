---
layout: markdown_page
title: "Blueprints"
---
inspired by [Engineering - Infrastructure Blueprint](/handbook/engineering/infrastructure/blueprint/)


## Overview

**Blueprints** are intended to flesh out our thinking about specific ideas we need to implement and/or problems we need to solve.

 Blueprints are sketches whose purpose is to foster and frame discussion around Infrastructure topics. 

As a guideline, a blueprint are structured as follows:

* Idea or problem statement: provide background and scope the idea or problem discussed in the blueprint
* Summary of past efforts (if applicable)
* Outline of options
* Additional considerations
  * Dependencies
  * Related blueprints or designs
  * Costs
* Recommendations

## Index

Listed alphabetically:

* [Data Needs - Stakeholder Interviews outcomes](/handbook/business-ops/blueprints/data-needs-outcomes)
* [Structure](/handbook/business-ops/blueprints/structure)
