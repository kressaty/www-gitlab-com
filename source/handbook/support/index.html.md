---
layout: handbook-page-toc
title: Support Team Handbook
---

## Welcome to the GitLab Support Team Handbook
{: .no_toc}

The GitLab Support Team provides technical support to GitLab.com and Self-Managed GitLab customers. The GitLab Support Team Handbook is the central repository for why and how we work the way we do.

If you are a customer or advocating for a customer who needs technical assistance, please take a look at the dedicated [Support Page](/support) which describes the best way to get the help you need and lists GitLab's paid service offerings.

If you are a GitLab team-member looking for some help, please see the [Internal Support for GitLab Team Members page](internal-support).

If you are a new Support Team member looking to get started, please take a look at the [onboarding section](#onboarding).

Know someone who might be a great fit for our team? Please refer them to the job descriptions below.

- [Support Engineer - Self Managed job description](/job-families/engineering/support-engineer).
- [Support Agent - GitLab.com job description](/job-families/engineering/dotcom-support/).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What does the Support Team do?

### We care for our customers

- Always assume you are the person responsible for ensuring success for the customer.
- When supporting a customer, any issue, incident or loss is _GitLab's loss_.
   - When a customer experiences trouble or downtime, take action with the same urgency you'd have if GitLab were experiencing downtime.
   - When a customer is losing productivity, take action with the same urgency you'd have if GitLab were losing productivity.
   - The rule of thumb is a customer down with 2,500 users gets the same urgency as if GitLab were losing $1,000,000 per day. This treatment is equal regardless of how much they are paying us.
- Escalate early.  Visibility across GitLab, up to and including the CEO, is always better earlier rather than later.  Ensure all resources needed are on the case for customers early.

### How we measure our performance 

We use [Key Performance Indicators](/handbook/support/performance-indicators/) (KPIs) to keep track of how well each Engineering Department is doing, including the Support team, as a whole.

The KPI measurements can be found under the `Reporting` tab in Zendesk if you have the appropriate access, but progress on meeting these KPIs is also tracked via the aforementioned KPI link and visually through reports in the [Support KPIs Dashboard](https://app.periscopedata.com/app/gitlab/421422/).

We review these KPIs weekly in the [Support Week-in-Review](/handbook/support/#support-week-in-review).


----

## Direction

The overall direction for Support in 2019 is set by our overall [strategic objectives](/strategy), with a particular emphasis on
- continued improvement of (Premium) Customer satisfaction.
- scaling the team through hiring and process improvements to pace ticket growth
- refining process, communication and team-training to improve efficiency as we scale

As can also be inferred from our [publicly visible OKR page](/company/okrs/), we anticipate 2019 will focus on the following elements:

### Ticket deflection through documentation

1. By building up a corpus of documentation informed by real-world problems we can ensure that customers can get the answers they need before they come into the queues.

   - *When learning, use the docs.*
   - *When troubleshooting, use the docs.*
   - *If something is missing, update the docs.*

1. By developing a [docs-first](https://docs.gitlab.com/ee/development/documentation/styleguide.html#docs-first-methodology) approach to answering, we can ensure that the documentation remains a highly useful [single source of truth](https://docs.gitlab.com/ee/development/documentation/styleguide.html#documentation-is-the-single-source-of-truth-ssot),
and that customers are more aware of where to find content on their own.

   - *Always respond with a link to the docs.*
   - *If docs content is missing, create it and link the customer to the MR.*

Beyond this work during specific support cases, the team is also asked to contribute their
expertise to GitLab's documentation by producing new guides and videos that would
be useful to our users — especially on emerging DevOps trends and use cases.

### Increase capacity & develop experts

In the past 3 years we expanded the support team significantly, and this trend will continue in 2019. As GitLab -- the product -- continues to expand, so will the skill and knowledge of all Support Engineers to be able to continue providing an excellent customer support experience.

1. Develop "Deep Dives", Bootcamps and other professional training programs to decrease the time it takes for a new team member to be effective.
1. Make sure that professional training sessions result in the production of publicly consumable resources:
docs fixes, tutorials and improvements to any related material.

### Experiment with support model

In late 2018 we moved from a "product-first" model to a "region-first" model. While this has affected the reporting structure, the lines
of work from the perspective of an individual Support team member continue to be aligned to product. In 2019 we want to experiment with a "concept-first"
model of support that will align individual strengths with customers needs regardless of which queue they come into.

Other areas we'd like to explore:
1. 24x7 capability beyond uptime support (i.e. weekend staffing)
1. Language support
1. US-only Federal Support

----

## Hazards & Challenges

### Team morale suffers

| Hazard                     | Commitment                              |
|----------------------------|-----------------------------------------|
| Ticket volume is too high | Build (and adjust) hiring model based on the best data available, hire as quickly as possible while keeping quality up. |
| Team knowledge doesn't match ticket difficulty | Develop training materials, provide time for professional development, encourage specializations.  |
| We aren't hitting our SLOs | Hire and train as per above. Add regularity to scheduling and encourage efficient work patterns.
| Leadership breaks trust    | Communicate in public channels, alert early and often of potential discussions, engender the GitLab value of Transparency. |
| Fear of conflict results in poor decisions | Provide focus on meta-issues by triaging issues in the [Active Now](#improving-our-processes---active-now-issue-board) board. Encourage buy-in and bravery. Truly listen and respond. Explicitly overrule: state reasoning and thank everyone for voicing opinions. |
| Team lacks commitment to results or implementing decisions | Ensure voices are heard. Re-enforce "disagree and commit". Build accountability. |
| There's no accountability in poor results or not meeting commitments | Reinforce GitLab value of Results by paying attention and following up. |
| Lack of trust as the team grows | Make an intentional effort to frequently do pairing sessions between regions.|

### Scaling

As we continue to increase in size, there's a number of challenges that we need to address and/or questions we need to answer:

As we grow:
1. What process inefficiencies do we need to address? (e.g. _Do all engineers need to know about every component/configuration/deployment strategy of GitLab, or are there ways we can specialize?_)
1. Which process inefficiencies do we need to intentionally keep because of a positive externality? (e.g. _Do we keep around a synchronous meeting because it helps build relationships?_)
1. What have we lost in scaling? How can we build it back in in a way that scales? (e.g. _Smaller groups lead to more ideation sessions, how can we make sure we're creating spaces for ideas to build off of each other?_)
1. How can we make sure we're broadcasting things appropriately so no one misses out on an important piece of information?

----

## Communications

The GitLab Support Team is part of the wider Engineering function. Be sure to check the
[communications section in the Engineering handbook](/handbook/engineering/#communication)
for tips on how to keep yourself informed about engineering announcements and initiatives.

Here are our most important modes of communication:

  * [Support Week in Review](#support-week-in-review). Important updates for everyone in support.
    You should try to check the SWIR at least once a week. If you have something to share with the
    entire team this is the best place to do it. For example, if you have an issue for a common bug, an issue that requires feedback,
    or an issue about an external project you're working.
  * [Slack channels](#slack) for ["informal"](/handbook/communication/#slack) communication.
    Due to our data retention policy in Slack, things shared there will eventually be deleted. If you want to share something there, please make sure it also has a more permanent place in our docs, handbook, issue tracker, etc.
  * [Meta issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues/) for any issues regarding workflows,
    general team suggestions, tasks or projects related to support, etc.

### GitLab.com

#### Groups
{: .no_toc}

We use the following groups to notify or add support team members to issues and merge requests on
GitLab.com.

| Group                                                                          | Who                      |
|--------------------------------------------------------------------------------|--------------------------|
| [@gitlab-com/support](https://gitlab.com/support)                              | All Support Team members |
| [@gitlab-com/support/amer](https://gitlab.com/support/amer)                    | AMER Support             |
| [@gitlab-com/support/apac](https://gitlab.com/support/apac)                    | APAC Support             |
| [@gitlab-com/support/emea](https://gitlab.com/support/emea)                    | EMEA Support             |
| [@gitlab-com/support/managers](https://gitlab.com/gitlab-com/support/managers) | All support managers     |

#### Projects
{: .no_toc}

Our team projects and issue trackers can be found in the
[Support parent group](https://gitlab.com/gitlab-com/support). Here are some selected projects
which are relevant to team communications.

| Project | Purpose |
|---------|---------|
| [support-team-meta](https://gitlab.com/gitlab-com/support/support-team-meta) | General conversations around support at GitLab |
| [support-training](https://gitlab.com/gitlab-com/support/support-training) | Pairing sessions, onboardings, and anything the support team uses to level up |
| [feedback](https://gitlab.com/gitlab-com/support/feedback) | Collects SSAT survey responses from Zendesk in the form of issues |
| [support-operations](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project) | Support Operations team project |


##### Support team meta issue tracker

We use the [Support meta issue tracker](#support-team-meta-issue-tracker) for tracking issues
and creating issues that may require feedback around support.

If you're interested in working on a project or task related to
support feel free to create an issue and link to any external issues or projects so that we can:

* Be transparent to the entire team what we're working on
* Have the opportunity to collaborate on external projects or tasks with other team members who are interested
* Avoid having team members do duplicate work

Issues regarding documentation or features for GitLab CE, EE or any of the GitLab
components should not go in this issue tracker, but in their appropriate issue tracker.

If you have a proposed solution that is actionable, it's best to [start a merge request](/handbook/communication/#everything-starts-with-a-merge-request), tag the team for feedback and link in the [Support Week in Review](#support-week-in-review).

### Slack

We follow GitLab's [general guidelines for using Slack](/handbook/communication/#slack)
for team communications. As only 90 days of activity will be retained, make sure
to move important information into the team handbook, product documentation,
issue trackers or customer tickets.

#### Channels
{: .no_toc}

| Channel                 | Purpose                                                   |
|-------------------------|-----------------------------------------------------------|
| `#support-team-chat`    | Support team lounge for banter, chat and status updates   |
| `#support_gitlab-com`   | Discuss GitLab.com tickets and customer issues            |
| `#support_self-managed` | Discuss self-managed tickets and customer issues          |
| `#support-managers`     | Discuss matters which require support managers' attention |
| `#spt-hiring`           | Discuss support team hiring-related matters               |

#### User Groups
{: .no_toc}

| Group                  | Who                       |
|------------------------|---------------------------|
| `@support-dotcom`      | GitLab.com Support Team   |
| `@support-selfmanaged` | Self-managed Support Team |
| `@support-team-apac`   | Support Team APAC         |
| `@support-team-emea`   | Support Team EMEA         |
| `@supportmanagers`     | Support Managers          |

If you need to be added to one or more of these groups, please open an issue in
the [access requests project](https://gitlab.com/gitlab-com/access-requests).


### Google Calendar

We use the following team calendars to coordinate events and meetings:

- GitLab Support
- [Support - Time off](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com) Add the following calendar by clicking on the "+" sign next to "other calendars", and choose "subscribe to calendar". Enter the ID `gitlab.com_as6a088eo3mrvbo57n5kddmgdg@group.calendar.google.com`

If you need access to these calendars, ask a support team member for help.


### Weekly Meetings

The Support Team has several meetings each week. These allow us to coordinate and
help us all grow together. Each meeting has its own agenda and is led by a different member of the team each week.

Discussion are encouraged to be kept in issues or merge requests so the entire team can collaborate, regardless of time zone.

Any demos or announcements that need to be shared with the entire team should be shared in the [Support Week in Review](#support-week-in-review).

|  Weekday  |   Region  |      Meeting Name     |                                        Purpose                                      |
|:---------:|:---------:|:---------------------:|:-----------------------------------------------------------------------------------:|
|  Tuesday  |    APAC   |      Support Call     |              Discuss metrics, demos, upcoming events, and ask questions             |
|  Tuesday  |    AMER   |  Casual Catch up |         Agendaless, a space for the team to banter and chat. If you have work to talk about, feel free.        |
| Wednesday | AMER,EMEA | Dotcom Support Call | GitLab.com support team meeting to discuss metrics, demos, upcoming events, and ask questions |
|  Thursday  |    EMEA   |      Support Call     |              Discuss metrics, demos, upcoming events, and ask questions             |

The regions listed above are the regions for which each call may be the most convenient, but all are welcome on any call. Every call is recorded and notes are taken on the agenda for each one. If you miss a meeting or otherwise can't make it, you can always get caught up.

All Zoom and agenda links can be found on the relevant calendar entry in the Support Calendar.

Past recordings of our meetings are [available on Google Drive](https://drive.google.com/drive/u/0/folders/0B5OISI5eJZ-DcnBxS05QUTdyekk).

#### Role of the Chair
{: .no_toc}
The main role of the chair is to start the meeting, keep the meeting moving along, and end the meeting when appropriate. There is generally little preparation required, but depending on the meeting, the chair will include choosing a "feature of the week" or similar. Please check the agenda template for parts marked as "filled in by chair."

During the meeting, the chair:

* will ensure that each point in the agenda is covered by the listed person,
* may ask the team to move a discussion to a relevant issue when appropriate,
* copy the agenda template for the following week and tag the next chair/secretary.

If a chair is not available, it is their responsibility to find a substitute.

#### Role of the Notetaker
{: .no_toc}
The notetaker should take notes during the meeting and if action is required, creates a comment and assigns it to the appropriate person.

If the notetaker is not available when it is their turn, they should find a substitute.

### Support Week in Review

Every Friday, we do a week in review, inspired by the [greater Engineering organization week in review](https://docs.google.com/document/d/1Oglq0-rLbPFRNbqCDfHT0-Y3NkVEiHj6UukfYijHyUs/edit).  You can add topics any time to the [support week in review google document](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit).

Any workflow changes or announcements should be shared in the SWIR and we recommend you check at least once a week to stay up to date on recent changes.
Ideally, the information shared here should have a permanent location such as an issue or merge request.

We encourage anyone in the team to share. We currently have the following topics:

* **Actionable**. For items that that requires a decision to be made or action to be taken (such as, asking for feedback on an issue).
* **Kudos**. Give a special kudos to other team members or highlight something they did.
* **Things to know about**. Share items that you would like to share with the team, like projects you're working on, known bugs, new workflows, cool articles you found, etc.
* **Metrics report**. Review the support metrics for the span of the week.
* **Music/TV/Movie Sharing**. Share and recommend a song, a show, or a movie with the team.
* **Personal Updates /  Weekend Plans / Random**. Share awesome pics from recent trips, what you're up to on the weekend, or something else.

On Fridays at 3pm EST, slackbot will remind the team with a link to the week in review and we can use a thread there for banter. Bear in mind that chat older than 90 days will no longer be retained.

You can read about how we got this started
[in this issue](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1394).

### Support Stable Counterparts
As a result of our direct interactions with customers, the Support Team occupies a unique position in GitLab where
we can connect project managers with the feedback they need to prioritize.
Each of the [DevOps Stages](/handbook/product/categories/#devops-stages) should have an IC counterpart, and each
section should have a manager counterpart.

Support Stable Counterparts, briefly, will:
- subscribe to the pertinent trackers / labels to be aware of new issues
- be especially aware of S1/S2 issues in their group, including workarounds. Issues likely to generate tickets should be surfaced to the wider team.
- attend product meetings in order to understand the priorities/challenges/upcoming features of the product group
- be aware of the issues raised by customers pertinent to their category, surfacing and advocating for them
- be a subject matter expert in the use of the features related to this group
- be the owner of any special processes or troubleshooting workflows that might pertain to the features in this group

Managers will:
- Fill in for team members who are out
- Facilitate communication between Support and the assigned group
- Help triage and broadcast important issues in weekly communications with the larger support team
- Catalyse training materials and sessions

If you're interested in becoming a stable counterpart for a group, please create an MR on `data/stages.yml` and `source/inclues/product/_categories.erb`
and assign to your manager.

At writing (Q2-FY20) ICs align to Stage/Product Manager rather than group to keep things easier for PMs.
Put another way: if the same product manager is in charge of multiple groups in a Stage we will not
consider assigning an additional stable counterpart.

### Cross Functional Meetings

Some functions don't fit as cleanly into the Support Stable Counterparts model.  As such we have standing meetings across functions to help surface issues and smooth the interface between such groups.

If you're missing from this list (and want to be on it) please let the Support Managers know in `#support-managers`

If you're on the Support Team and have something you'd like to surface, or would like to attend one of these meetings, feel free to post in `#support-managers`.


| Section    | Group               | Group Contact       | Support Manager     | Support Counterpart        | Frequency                         |
|:----------:|:-------------------:|:-------------------:|:-------------------:|:--------------------------:|:---------------------------------:|
| UX         | UX                  | Christie Lenneville | Lyle Kozloff        | Cynthia Ng                 | weekly team meeting |
| UX         | Docs/Tech Writing   | Mike Lewis          | Tom Atkins          | Cynthia Ng & Drew Blessing | N/A |
| Production | .com Infrastructure | Dave Smith          | Lyle Kozloff        | Vlad Stoianovici           | every 2 weeks |
| Security   | Abuse               | Roger Ostrander     | Lyle Kozloff        | TBD                        | N/A |
| Security   | Security Operations | Jan Urbanc          | Lyle Kozloff        | TBD                        | N/A |
| Performance| Performance         | Stan Hu             | Lee Matos           | N/A                        | N/A |
| Legal      | Legal               | Jaimie Hurewitz     | Lyle Kozloff        | N/A                        | N/A |
| Finance    | Budget              | Chase Wright        | Tom Cooney          | N/A                        | 1x Qtr on budget + once per month |
| Finance    | Accounts            | Cristine Tomago     | TDB                 | N/A                        | N/A |
| PeopleOps  | Recruiting          | Cyndi Walsh         | Tom Cooney          | N/A                        | weekly |
| PeopleOps  | After-hire care     | Jessica Mitchell    | Tom Cooney          | N/A                        | every 2 weeks |
| Sales      | Sales               | TBD                 | Tom Atkins          | N/A                        | weekly on Fri join EMEA scrum |
| Sales      | Customer Success    | Kristen Lawrence    | Tom Atkins          | N/A                        | weekly on Fri join EMEA scrum |
| Sales      | Community Relations | David Planella      | Tom Atkins          | N/A                        | N/A |
| Meltano    | Meltano             | Danielle Morrill    | Jason Colyer        | TBD                        | N/A |


## Processes

### Support Workflows

- [Support Workflows](/handbook/support/workflows)
   - [How to Work with Tickets](/handbook/support/workflows/working-with-tickets.html)
   - [How to Submit issues to Product/Development](/handbook/support/workflows/issue_escalations.html)
   - [How to Submit Code to the GitLab Application](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md)
   - [How to Submit Docs when working on customer issues](/handbook/documentation) (see 'docs-first methodology')

### Zendesk Instances
At GitLab, the Support Team currently manages 2 different [Zendesk Instances](/handbook/support/workflows/zendesk-instances.html):

- [GitLab Support Instance](gitlab.zendesk.com)
- [GitLab US Federal Support Instance](federal-support.gitlab.com)

All our Support Agents and Engineers have access to the GitLab Support Instance, whereas only Support Engineers who are US Citizens have access to the GitLab US Federal Support Instance.

### Time Off
The Support Team follows [GitLab's paid time off policy](/handbook/paid-time-off). However, do note that if a large number of people are taking the same days off, you may be asked to reschedule. If a holiday is important to you, please schedule it early.

In addition to the tips in [Communicating Your Time Off](/handbook/paid-time-off/#communicating-your-time-off) please also:
- link your personal Google calendar to [PTO Ninja](/handbook/paid-time-off/#pto-ninja)
  so a 'Busy' event will block your calendar automatically. This also serves
  to block your availability in Calendly.
- link [PTO Ninja](/handbook/paid-time-off/#pto-ninja) to the [**Support - Time Off**](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com)
  calendar so PTO Ninja will add an entry automatically
- if you're on-call, make sure you have coverage and that PagerDuty is up to date
- reschedule any 1:1 meetings with your manager
- add an agenda item during the team meeting before your time off
- add an agenda item after you get back to let us know what you've been up to!

If you do not have access to the [**Support - Time Off**](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com) team calendar, please raise it in the `#support-team-chat` channel on Slack and someone will share it with you.

You do **not** need to add time-off events to the shared calendar if you'll be gone less than half a day. *Do* consider blocking off your
own calendar so that customer calls or other meetings won't be scheduled during your time away.

If you need to go for a run, grab a coffee or take a brain break please do so without hesitation.

#### Linking PTO Ninja to your personal and team calendar

1. In Slack, click the `+` sign next to 'Apps' at the bottom of the left sidebar
1. Search for 'PTO Ninja' and click 'View'
1. Type 'help' and press enter
1. PTO Ninja will reply with some options. Click 'Settings'
1. In the dropdown next to 'Here are your profile settings:' choose 'Calendar'
1. Under 'Your synced calendar' click the button to sync your personal calendar
1. After your personal calendar is linked, click 'Add calendar' under
   'Additional calendars to include?'. The 'Support - Time Off' calendar ID is
   `gitlab.com_as6a088eo3mrvbo57n5kddmgdg@group.calendar.google.com`

### Onboarding

- [Support Onboarding](/handbook/support/onboarding)
   - [Support Engineer Onboarding Checklist](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20Self-managed%20Support%20Engineer.md)
   - [GitLab.com Support Agent Onboarding Checklist](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20GitLab.com%20Support%20Agent.md)

### What if I Feel Threatened or Harassed While Handling a Support Request?
Just as Support Team are expected to adhere to the Code of Conduct, we also expect customers to treat the Support Team
with the same level of respect.

If you receive threatening or hostile emails from a user, please create a confidential issue in the GitLab Support Issue Tracker
and include:

1. A history of the user's communication
1. Relevant links
1. Summary of the high-level issues
1. Any other supporting information

Include your Manager, Director of Support, Abuse Team and Chief People Officer in this issue. Together we will evaluate on a
case-by-case basis whether to take action (e.g. ban the user from the forums for violating the [Code of Conduct](/handbook/people-group/code-of-conduct)).

### Improving our processes - 'Active Now' issue board
The Support team use ['support-team-meta' project issues](https://gitlab.com/gitlab-com/support/support-team-meta/issues/) to track ideas and initiatives to improve our processes. The ['Active Now' issue board](https://gitlab.com/gitlab-com/support/support-team-meta/boards/580661) shows what we're currently working on. It uses three labels:

1. **Blocked** - waiting for another team or external resource before we can move ahead
1. **Discussing this week** - under active discussion to arrive at a decision
1. **In Progress** - actively being worked on

Some principles guide how these labels are used:

1. A **maximum of six issues** at any time for each label (18 total issues)
1. All issues with one of the above labels must be **assigned** to one or more support team members
1. All issues with one of the above labels must have a **due date** no longer than a week ahead
1. If an issue is too big to complete in a week it should be **split into smaller parts that can be completed in a week** (a larger 'parent' issue is OK to keep in the project, but it shouldn't make it onto the 'In Progress' column)

**Each week we look at the board and discuss the issues to keep things moving forward.**

By keeping a maximum of six issues for each label, we **limit work in progress** and make sure things are completed before starting new tasks.

**Adding and managing items on the board:**

Support managers will regularly review the board to keep items moving forward.

1. The team can **vote on issues not on the board** by giving a 'thumbs up' emoji so we can see [popular issues](https://gitlab.com/gitlab-com/support/support-team-meta/issues?sort=popularity&state=opened).
1. Support managers will look at popular issues and add them to the board when there is room.
1. Support managers will **curate** issues to prevent a large backlog. Unpopular or old issues can be closed / merged to keep the backlog manageable.

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Support Resources

### Handbook links

- [GitLab Team page](/company/team/)
- [Product Categories](/handbook/product/categories) - Find out what team handles what

- [Support Hiring &amp; Managers](/handbook/support/managers)
- [Support Channels](/handbook/support/channels)
- [On-Call](/handbook/on-call/)
- [Support Ops](/handbook/support/support-ops)
- [Advanced Topics](/handbook/support/advanced-topics)

### Documentation

- GitLab
  - [GitLab.com Status](https://status.gitlab.com/)
  - [GitLab Releases](/blog/categories/releases/)
- Writing docs
  - [GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/index.html)
  - [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html)
  - [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- Setting up GitLab
  - [GitLab Architecture Overview](https://docs.gitlab.com/ee/development/architecture.html)
  - [Requirements](https://docs.gitlab.com/ee/install/requirements.html)
  - [Installation methods for GitLab](/install/)
  - [Backing up and restoring GitLab](https://docs.gitlab.com/ee/raketasks/backup_restore.html)
  - [Omnibus configuration settings](https://docs.gitlab.com/omnibus/settings/README.html)
  - [Omnnibus Configuration options](https://docs.gitlab.com/omnibus/settings/configuration.html)
  - [Omnibus Database settings](https://docs.gitlab.com/omnibus/settings/database.html#seed-the-database-fresh-installs-only)
- Debugging GitLab
  - [Log system](https://docs.gitlab.com/ee/administration/logs.html)
  - [Rake tasks](https://docs.gitlab.com/ee/raketasks/README.html)
  - [Maintenance Rake Tasks](https://docs.gitlab.com/ee/administration/raketasks/maintenance.html)
  - [Debugging Tips](https://docs.gitlab.com/ee/administration/troubleshooting/debug.html)
  - [Debugging resources for GitLab Support Engineers](https://docs.gitlab.com/debug/)
  - [GitLab Rails Console Cheat Sheet](https://docs.gitlab.com/debug/gitlab_rails_cheat_sheet.html)
- GitLab features
  - [Install GitLab Runner](https://docs.gitlab.com/runner/install/)
  - [GitLab CI example projects](https://gitlab.com/gitlab-examples)
  - [Elasticsearch](https://docs.gitlab.com/ee/integration/elasticsearch.html)
  - [Connecting GitLab with a Kubernetes cluster](https://docs.gitlab.com/ee/user/project/clusters/)
- Developing GitLab
  - [GitLab development utilities](https://docs.gitlab.com/ee/development/utilities.html)
  - [Feature flags](https://docs.gitlab.com/ee/development/feature_flags.html)
  - [What requires downtime?](https://docs.gitlab.com/ee/development/what_requires_downtime.html)

### Internal tools

- [Support Toolbox](https://gitlab.com/gitlab-com/support/toolbox) - Includes tools such as `json_stats` (analyze JSON logs), `strace_parser` (analyze `strace` output), `gitlabsos` (get all logs and other data from customers), etc.
- [Dev Resources](https://gitlab.com/gitlab-com/dev-resources) - Create a test instance
- [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
- [License App](https://license.gitlab.com/) - Details of all licenses
- [Customers portal admin](https://customers.gitlab.com/admins)
- [GitLab Regressions](https://regressions.gitlab.io/)


### External tools

- [ExplainShell](https://explainshell.com/) - Break down a terminal command
