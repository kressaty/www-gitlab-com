---
layout: markdown_page
title: "Engineering efforts by People Ops"
---
## On this page
{:.no_toc}

- TOC
{:toc}

### Source of truth

For all automations mentioned in this page, BambooHR database acts as the single
souce of truth. We make use of a [fork](https://gitlab.com/gitlab-com/people-ops/bamboozled/)
of [Bamboozled](https://github.com/Skookum/bamboozled) Ruby gem, and interact
with BambooHR using an bot user with limited access to employee details.

#### Data confidentiality

The bot user used for automations has read-only access to the following employee
details. We actively ensure no extremely confidential information is accessible
through this bot user.

TODO: Add list of fields accessible by the bot.

### Partially automated processes

#### Onboarding issue creation

PeopleOps specialists make use of GitLab's
[chatops](https://docs.gitlab.com/ee/ci/chatops/) functionality to automate
creation of onboarding issue containing onboarding tasks relevant to the
incoming team member. The slack command used for this is

```
/pops run onboarding <id_in_BambooHR_URL>
```

The Slack command triggers a pipeline in the `employment` project, which will
run the job `onboarding`, and reply with a link to the newly created onboarding
issue. The onboarding issue will be automatically assigned to the PeopleOps
Specialist who ran the command, and the incoming team member's Manager.

The onboarding tasks that are applicable to all team members are listed in the
general [`onboarding.md`](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md)
file. It will be included by default in the onboarding issue.

The job then grabs various details of the incoming team member, like country of
residence, entity through which they are hired, division, department, job title
etc. Then for each of this detail, it checks for the existence of a task file in
the [`onboarding_tasks` folder](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md)
of the `employment` project. These tasks files are of the format
`country_<country name>.md`, `entity_<entity name>.md`, `division_<division name>.md`,
`department_<department name>.md`, etc. If such a file is found, it includes
contents of those files also in the onboarding issue.

`Note:` Due to some technical difficulties, the onboarding issue created by the
command will contain onboarding tasks applicable to various job families at
GitLab. PeopleOps specialists will have to manually delete all of them except
the one applicable to the incoming team member. Discussions for improving this
situation is happening [here](https://gitlab.com/gitlab-com/people-ops/people-operations-engineering/issues/5).

### Fully automated processes

PeopleOps make use of GitLab's [scheduled pipelines] features to automate few
Slack announcements related to team member events like hiring and anniversaries.
These are configured in the
[`employment`](https://gitlab.com/gitlab-com/people-group/employment/pipeline_schedules)
project where the codebase implementing it resides.

A slack bot named `PeopleOps Bot` is configured to post these announcements to
necessary slack channels.

#### Anniversary announcements

A scheduled pipeline is configured to automatically send a message
congratulating all team members celebrating a work anniversary that week to the
slack channel `#team-member-updates`. The message will contain list of all such
team members and the number of years they are celebrating at GitLab.

Currently, the pipeline is scheduled to be run at 10:00 AM UTC on every
Thursday.

#### New hire announcements

A scheduled pipeline is configured to automatically send a message containing a
list of all new team members who are joining GitLab in the following week. It
includes details like name, email address, joining date, and their job title.
The message also includes a link to the [periscope chart]() containing a
detailed breakdown and overview of the hiring process over time.

For the time being, this message is sent to the slack channel
`#peopleops-alerts`, and is manually copy-pasted to `#team-member-updates` by a
PeopleOps specialists after manual verification of the details. Once the
onboarding process is streamlined enough, this manual work will be removed and
the bot will directly post the message to `#team-member-updates`.

Currently, the pipeline is scheduled to be run at 04:00 PM UTC on every
Thursday.

#### Informing PeopleOps Specialists about details missing in BambooHR for upcoming new hires

For the new hire announcements to be accurate, it is required to ensure the
BambooHR details of team members joining the following week is as complete as
possible. To help PeopleOps team in this task, another scheduled pipeline is
run to verify if the BambooHR details of all incoming team members is complete.
This pipeline notifies PeopleOps specialists in `#peopleops-alerts` channel
about people whose details are missing and the details that are missing for each
person.

Since PeopleOps Specialists should have enough time to fix these missing
details before new hire announcements are sent, it is necessary this job should
be run an adequate amount of time before the new hire announcements job is run.
Currently, the pipeline is scheduled to be run at 02:00 PM on every Wednesday.

#### Closing outdated onboarding issues

It is expected that onboarding issues be completed and closed within 30 days of
opening. To remind team members about this, we are using the `due date`
functionality of GitLab issues. When an onboarding issue is created, we
automatically set a due date of 35 days to it (we open onboarding issues the
week before the team meber joins. So 35 days of due date will give them almost
30 days to complete onboarding tasks after they actually start at GitLab).
GitLab will send a reminder notification email to all the assignees of the issue
about the nearing due date.

In addition to this due date, team members get an additional 30 days to complete
and close the onboarding issue. So in total, a team member gets aroudn 60 days
to complete their onboarding issue.

We have another scheduled pipeline to close the outdated issues (issues that
have been open for more than 60 days). This pipeline will add a comment on the
issue that it is being automatically closed and what the team members should do
if they have onboarding tasks remaining.

Currently, the pipeline is scheduled to be run at 09:30 PM on every Friday. It
will close all the onboarding issues created before 60 days from that date.
