---
layout: handbook-page-toc
title: "Infrastructure"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Common Links

| **GitLab.com Status** | [**Status Page**](https://status.gitlab.com/)
| **How to get help** | [**How to get help**](/handbook/engineering/infrastructure/production/#how-to-get-help)
| **Incident Management** | [How we handle GitLab.com incidents](/handbook/engineering/infrastructure/incident-management/) |
| **Change Management**   | [How we manage changes to GitLab.com](/handbook/engineering/infrastructure/change-management/) |

| **Workflow** | [**How may we be of service?**](production/#workflow--how-we-work) | | |
| **Issue Trackers** | [**Infrastructure**](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/): [Milestones](https://gitlab.com/gitlab-com/gl-infra/infrastructure/milestones), [OnCall](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=oncall) | [**Production**](https://gitlab.com/gitlab-com/gl-infra/production/issues/): [Incidents](https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=incident), [Changes](https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=change), [Deltas](https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=delta) | [**Delivery**](https://gitlab.com/gitlab-com/gl-infra/delivery)
| **Slack Channels** | [#infrastructure-lounge](https://gitlab.slack.com/archives/infrastructure-lounge), [#database](https://gitlab.slack.com/archives/database) | [#alerts](https://gitlab.slack.com/archives/alerts), [#production](https://gitlab.slack.com/archives/production) | [#g_delivery](https://gitlab.slack.com/archives/g_delivery)
| **Operations** | [Runbooks](https://gitlab.com/gitlab-com/runbooks) (please contribute!) | **On-call**: [Handover Document](https://docs.google.com/document/d/1IrTi06fUMgxqDCDRD4-e7SJNPvxhFML22jf-3pdz_TI), [Reports](https://gitlab.com/gitlab-com/gl-infra/production/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=oncall%20report) |

## Other Pages
{:.no_toc}

| **GitLab.com** | [Architecture](/handbook/engineering/infrastructure/production-architecture/) | [Environments](/handbook/engineering/infrastructure/environments/) | [Monitoring](/handbook/engineering/monitoring/) | [Performance](/handbook/engineering/performance/) |
| **Production** | [SRE Onboarding](/handbook/engineering/infrastructure/sre-onboarding/) | [Readiness Guide](https://gitlab.com/gitlab-com/gl-infra/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md) | [Database Reliability](/handbook/engineering/infrastructure/database/) | [On-call Handover](/handbook/engineering/infrastructure/on-call-handover/) |

- [Production team handbook](/handbook/engineering/infrastructure/production/)
- [GitLab.com and GitLab Hosted data breach notification policy](/security/#data-breach-notification-policy)
- [Interning with SRE](handbook/engineering/infrastructure/career/)

## Mission

The **Infrastructure Department** is the primary responsible party for the **availability**,
**reliability**, **performance**, and **scalability** of all user-facing services (most
notably **GitLab.com**, the largest production GitLab Installation on the planet). Other
departments and teams contribute greatly to these attributes of our service as well. In these
cases it is the responsibility of the Infrastructure Department to close the feedback loop
with monitoring and metrics to drive accountability.

## Vision

We are a blend of operations gearheads and software crafters that apply sound engineering
principles, operational discipline and mature automation to make GitLab.com ready for
mission-critical customer workloads. We strive for excellence every day by living and
breathing [**GitLab's values**](/handbook/values/) as our guiding operating principles in
every decision we make and every action we take.

## Design

The [**Infrastructure Library**](./library/) contains documets that outline our thinking about the problems we are solving and represents the ***current state*** for any topic, playing a significant role in how we produce technical solutions to meet the challenges we face.

**Blueprints** scope out our initial thinking about specific problems and issues we are working on. **Designs** outline the specific architecture and implemetation.

## OKRs

GitLab uses **Objectives and Key Results** (OKRs) as *[quarterly goals](/company/okrs/) to execute our strategy to make sure [said] goals are clearly defined and aligned throughout the organization*. We capture our OKRs through [issues](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/boards/1240891?label_name[]=Infrastructure%20mStaff).

## Meetings

GitLab is a widely distributed company, and we aim to work asynchronously most of the time. There are times, however, when we must get *together* to discuss topics in real time, and thus, we do have some meetings scheduled. Infrastructure has four primary [**meetings**](/handbook/engineering/infrastructure/meeting/).

## Teams

The Infrastructure Department is comprised of four teams teams:

* Three [**Reliability**](team/reliability/) teams, which operate all user-facing GitLab service.
* [**Delivery**](team/delivery/), which focuses on GitLab's delivery of software releases to GitLab.com and the public at large
* [**Scalability**](team/scalability/), which focuses on GitLab'nd GitLab.com at scale

For details on the Department's structure, see the [**Infrastructure Teams Handbook section**](team/).

Additionally, [**Infrastructure's mStaff**](mstaff/) is the loose denomination for the group of people who report directly to the Director of Engineering, Infrastructure, a group composed of both managers and individual contributors responsible for the overall direction of Infrastructure

### SRE Stable Counterparts and areas of ownership

Every SRE is aligned with an engineering team. Each SRE can help the teams at each stage of the process. Planning, discovery, implementation, and further iteration. The area an SRE is responsible for is part of their title, e.g. "SRE, Plan, Monitor." You can see which area of the product each SRE is aligned with in the [team org chart](/company/team/org-chart/).

Multiple SREs are aligned with areas of the product. This area will be listed on the [team page](/company/team/) under their title as an expertise or specialty, e.g. "Plan expert."  This way there is a team of SREs available to provide help in the case that another is out of the office or busy with another incident or team.

There are 2 dimensions of ownership for the SRE teams.  First along Product/Engineering team lines and second around infrastructure needs.

GitLab Product/Service to Infrastructure Label mapping:

|Section | Stages | Label(s) | Team |
|---------------|--------|---------------|----------|
|Dev            | Create, Plan, Manage        | ~Product:create, ~Product:Plan, ~Product:Manage   | Dev & Ops Infra team (Jose) |
|Ops            | Monitor, Configure          | ~Product:Monitor, ~Product:Configure              | Secure & Defend Infra team (Anthony) |
|CI/CD          | Verify, Package, Release    | ~Product:Verify, ~Product:Release                 | CI/CD & Enablement Infra team (Dave) |
|Sec            | Secure      | ~Product:Secure    | Secure & Defend Infra team (Anthony) |
|Defend         | Defend      | ~Product:Defend    | Secure & Defend Infra team (Anthony) |
|Enablement & Growth | Many                    |  ~Service:Singleton                  | CI/CD & Enablement Infra team (Dave) |

*** Security is an aspect of all three teams so a relationship exists for all 3 teams.

Operational/Infra axes of alignment:
1. Observability: Metrics/Alerting (Prometheus, Grafana, PagerDuty, Pingdom) - Dev & Ops Infra team (Jose)
2. Observability: Logging (ELK + log shipping design) - Secure & Defend Infra team (Anthony)
3. Infrastructure tooling:  Chef, Terraform - Secure & Defend Infra team (Anthony)
4. Core Database: Postgres backups, tuning/configuration - Dev & Ops Infra team (Jose)
5. Backup Tooling processes and testing: Git (CI/CD & Enablement Infra team (Dave)) and Postgres Data(Dev & Ops Infra team (Jose))
