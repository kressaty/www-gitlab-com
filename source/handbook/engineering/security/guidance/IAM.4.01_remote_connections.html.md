---
layout: handbook-page-toc
title: "IAM.4.01 - Remote Connections Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# IAM.4.01 - Remote Connections

## Control Statement

Remote connections to production systems are controlled with a combination of SSH and multi-factor authentication.

## Context

Where and only if applicable, appropriate, and technically feasible, and with the understanding GitLab is a cloud-native, fully-remote and international organization favoring a Zero Trust network without a traditional corporate network infrastructure, access will be managed with a combination of [ssh](https://nvlpubs.nist.gov/nistpubs/ir/2015/NIST.IR.7966.pdf) and [Multi-factor Authentication (MFA)](https://csrc.nist.gov/glossary/term/Multi_Factor-Authentication) technologies.

## Scope

This control applies to all systems within our production environment. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.

## Ownership

Control owner: 
* `Security Management`

Process owner: 
* Security Operations
* IT-Ops

## Guidance

Identity access management systems should enforce SSH or MFA for connections to production systems and networks.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Remote Connections control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/823).

### Policy Reference

## Framework Mapping

* ISO
  * A.11.2.6
* SOC2 CC
  * CC6.6
  * CC6.7
* PCI
  * 8.1.1
  * 8.6
