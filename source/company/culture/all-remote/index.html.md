---
layout: markdown_page
title: "All Remote"
---

## On this page
{:.no_toc}

- TOC
{:toc}

GitLab is an all-remote company with [team members](/company/team/) located in more than 60 countries around the world.

On this page and subpages, we'll share what "all remote" really means, [how it works at GitLab](/company/culture/all-remote/tips/#how-it-works-at-gitlab), some [tips and tricks](/company/culture/all-remote/tips/#tips-for-leaders-and-other-companies) for remote teams, and [resources](/company/culture/all-remote/resources/) to learn more.

## The Remote Manifesto

All-remote work promotes:

1. Hiring and working from all over the world *instead of* from a central location.
1. Flexible working hours *over* set working hours.
1. Writing down and recording knowledge *over* verbal explanations.
1. Written down processes *over* on-the-job training.
1. Public sharing of information *over* need-to-know access.
1. Opening up every document for editing by anyone *over* top-down control of documents.
1. Asynchronous communication *over* synchronous communication.
1. The results of work *over* the hours put in.
1. Formal communication channels *over* informal communication channels.

## Why remote?

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NoFLJLJ7abE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

>  **"Remote is not a challenge to overcome. It's a clear business advantage."** -Victor, Product Manager, GitLab

From the cost savings on office space to more flexibility in employees' daily lives, all-remote work offers a number of advantages to organizations and their people.

But we also recognize that being part of an all-remote company isn't for everyone. Here's a look at some of the advantages and disadvantages.  

## Our long-term vision for remote work

Learn more about GitLab's [long-term vision for remote work](/company/culture/all-remote/vision/), and why we can embrace the future of work right now.

## How we built our all-remote team

As GitLab has grown, we've learned a lot about what it takes to build and manage a fully remote team, and want to share this knowledge to help others be successful.

Find out [how GitLab makes it work](/company/culture/all-remote/tips/#how-it-works-at-gitlab).

## Advantages and benefits

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Xw-31PZkHOo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
Operating in an all-remote environment provides a multitude of benefits and competitive advantages for employees, employers, and the world. 

Learn more about [benefits and advantages to operating in an all-remote environment](/company/culture/all-remote/benefits/).

## Disadvantages

Despite its many [advantages](/company/culture/all-remote/benefits/), all-remote work isn't for everyone. It can have disadvantages for potential employees depending on their lifestyle and work preferences, as well as the organization.

Learn more about [disadvantages to all-remote, along with solutions to these challenges](/company/culture/all-remote/drawbacks/).

## People

All-remote organizations tend to attract people who place a high degree of value on autonomy, flexibility, empathy, and mobility. It also presents outsized opportunity for people who must live or prefer to live in rural areas, where well-paying careers in technical industries are few and far between.

Learn more about the [types of people who are adopting a remote lifestyle](/company/culture/all-remote/people/).

## Values

While all-remote isn't a value itself, it's something we do in order to practice our values.

Learn how a collection of values at GitLab [contributes to a thriving all-remote environment](/company/culture/all-remote/values/). 

## Jobs

Job seekers are wise to point their efforts towards companies that are built to support 100% remote. You're able to bypass hours of lobbying for a remote arrangement during the interview process, and you're assured that the tools you need to operate effectively from anywhere will be included from the get-go. 

Learn more about [all-remote and remote-first organizations leading the way,  job boards that curate high-quality remote roles, and informal job searching tactics](/company/culture/all-remote/jobs/).

## How to evaluate a remote role

Not every every remote job is created equal. Learn more about [considerations and questions to ask when evaluating a remote role](/company/culture/all-remote/evaluate/).

## Getting started in a remote role

Learn more about [considerations and tips for starting a new remote role](https://about.gitlab.com/company/culture/all-remote/getting-started/). 

## Management

Managing an all-remote company is much like managing any other company. It comes down to trust, communication, and company-wide support of shared goals.

Learn more about [what's required to effectively and efficiently manage an all-remote company](/company/culture/all-remote/management/).

## Scaling

GitLab believes that all-remote is the future of work, and that it not only works well at scale, but works *better* at scale than antiquated colocated models. 

Learn more about [challenges, solutions, and benefits of all-remote at scale](/company/culture/all-remote/scaling/). 

## Stages of remote work

Learn more about the [various stages of remote work](/company/culture/all-remote/stages/), from no remote to all-remote.

## Tips for working remotely

Building a remote team or starting your first all-remote job? Check out our [tips for working remotely.](/company/culture/all-remote/tips)

## Resources

Browse our [resources page](/company/culture/all-remote/resources) to learn more about GitLab's approach, read about remote work in the news, and see what other companies are leading the way.

We've also compiled a [list of companies](/handbook/got-inspired/) that have been inspired by GitLab's culture.

## Hiring

GitLab envisions a world where talented, driven individuals can find roles and seek employment based on business needs, rather than an oftentimes arbitrary location. 

Hiring across the globe isn't without its challenges. There are local regulations and risks unique to countries and regions around the globe. We believe that these challenges are worth overcoming, and opening our recruiting pipeline beyond the usual job centers creates a competitive advantage. We hope to see this advantage wane as more all-remote companies are created. 

Learn more about [hiring in an all-remote environment](/company/culture/all-remote/hiring/).

## Compensation

While there are certain complexities to paying team members who are spread out in over 50 countries, we believe that it's worthwhile. Being an all-remote company enables us to [hire the world's best talent](/company/culture/all-remote/hiring/), not just the best talent from a few cities.

Learn more about [compensation in an all-remote environment](/company/culture/all-remote/compensation/).

## Learning and Development

We believe that all-remote companies are at a competitive advantage when it comes to educating and developing team members.

Learn more on how to make [learning and development a companywide mindset in an all-remote environment](/company/culture/all-remote/learning-and-development/).

## Informal Communication

In an all-remote environment, informal communication should be formally addressed. Leaders should organize informal communication, and to whatever degree possible, design an atmosphere where team members all over the globe feel comfortable reaching out to anyone to converse about topics unrelated to work.

Learn more about [enabling informal communication in an all-remote company](/company/culture/all-remote/informal-communication/).

## Meetings

Learn how to decide when a meeting is necessary and [how to optimize them in an all-remote environment](/company/culture/all-remote/meetings/). 

## Workspace

All-remote enables the creation of a custom office, perfectly tailored for your desires and ergonomic needs. 

Learn more about [key considerations when building and evolving your remote workspace](/company/culture/all-remote/workspace/).

## Stories

Read the [stories](/company/culture/all-remote/stories/) of some of our team members and hear how remote work has impacted their lives.

## Interviews

Read and listen to [interviews](/company/culture/all-remote/interviews/) on the topic of working remotely, hosted by GitLab team members. 

## History

Learn about [historical milestones, inflection points, and prescient interviews](/company/culture/all-remote/history/) in the evolution and expansion of remote work.

## Hybrid-remote

Hybrid-remote companies have one or more offices where a subset of the company commutes to each day, paired with a subset of the company that works remotely. 

Learn more about [advantages, challenges, and the primary differences between all-remote and other forms of remote working](/company/culture/all-remote/part-remote/).

## Working while traveling

Working remotely enables a tremendous amount of freedom, enabling team members to work from anywhere so long as there is a reliable internet connection.

Learn more about [optimizing comfort and efficiency when taking your office with you while traveling](/company/culture/all-remote/working-while-traveling/).

## Remote work conferences, summits, and events

Particularly for those who are seeking a new role with an all-remote or remote-first company, events can be a great place to meet others who have experience and connections in the space.

Learn more about [remote work conferences and summits, the power of networking, and the unique elements of experiencing a virtual event](/company/culture/all-remote/events/).

## All-remote Pick Your Brain interviews

If people want advice on structuring or managing an all-remote organization, we'd love to connect. Learn more about [requesting a Pick Your Brain interview on all-remote](/company/culture/all-remote/pick-your-brain/).

## Contribute to this page

At GitLab, we recognize that the whole idea of all-remote organizations is still
quite new, and can only be successful with active participation from the whole community.
Here's how you can participate:

- Propose or suggest any change to this site by creating a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/).
- [Create an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues/) if you have any questions or if you see an inconsistency.
- Help spread the word about all-remote organizations by sharing it on social media.
